<?php

/**
 * This is the model class for table "cloud_admin".
 *
 * The followings are the available columns in table 'cloud_admin':
 * @property integer $userid
 * @property string $username
 * @property string $password
 * @property string $lastloginip
 * @property string $lastlogintime
 * @property string $email
 * @property string $realname
 */
class Admin extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Admin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cloud_admin';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username', 'length', 'max'=>20),
			array('password', 'length', 'max'=>32),
			array('lastloginip', 'length', 'max'=>15),
			array('lastlogintime', 'length', 'max'=>10),
			array('email', 'length', 'max'=>40),
			array('realname', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('userid, username, password, lastloginip, lastlogintime, email, realname', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userid' => 'Userid',
			'username' => 'Username',
			'password' => 'Password',
			'lastloginip' => 'Lastloginip',
			'lastlogintime' => 'Lastlogintime',
			'email' => 'Email',
			'realname' => 'Realname',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('userid',$this->userid);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('lastloginip',$this->lastloginip,true);
		$criteria->compare('lastlogintime',$this->lastlogintime,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('realname',$this->realname,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}