<?php

/**
 * This is the model class for table "cloud_article".
 *
 * The followings are the available columns in table 'cloud_article':
 * @property integer $id
 * @property string $title
 * @property string $summary
 * @property string $detail
 * @property integer $isrecommend
 * @property integer $author
 * @property string $update_time
 * @property string $create_time
 */
class Article extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Article the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cloud_article';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title,summary', 'required', 'message' => '{attribute}不能为空'),
            array('isrecommend,iscover,author', 'numerical', 'integerOnly' => true),
            array('title,content,summary', 'match', 'pattern' => '/^[\x{00}-\x{ff}a-zA-Z\d]{2,}$/', 'message' => '请输入正确的{attribute}'),
            array('title', 'length', 'max' => 64, 'message' => '{attribute}长度超出限制'),
            array('summary', 'length', 'max' => 256, 'message' => '{attribute}长度超出限制'),
            array('image', 'length', 'max' => 128, 'message' => '{attribute}长度超出限制'),
            array('content, create_time,update_time,category,author', 'safe'),
            array('id, title, summary, category,content, isrecommend, author, update_time, create_time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {

        return array(
            'enum' => array(self::BELONGS_TO, 'Enum', 'category'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function scopes() {
        return array(
            'recently' => array(
                'order' => 'isrecommend DESC,create_time DESC',
                'limit' => 5,
            ),
        );
    }

    public function recently($limit = 5) {
        $this->getDbCriteria()->mergeWith(array(
            'order' => 'isrecommend DESC,create_time DESC',
            'limit' => $limit,
        ));
        return $this;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => '标题',
            'summary' => '摘要',
            'category' => '类型',
            'content' => '内容',
            'image' => '图片路径',
            'isrecommend' => '置顶',
            'iscover' => '设为封面',
            'author' => '作者',
            'update_time' => '更新时间',
            'create_time' => '创建时间',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {

        $criteria = new CDbCriteria;
        $criteria->with = "enum";
        $criteria->compare('t.id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('summary', $this->summary, true);
        $criteria->compare('category', $this->category);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('isrecommend', $this->isrecommend);
        $criteria->compare('author', $this->author);
        $criteria->compare('update_time', $this->update_time, true);
        $criteria->compare('create_time', $this->create_time, true);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('attributes' => array(
                    'create_time', 'category' => array('asc' => ' enum.id asc ', 'desc' => ' enum.id desc '), 'iscover' => array('asc' => 'iscover asc ', 'desc' => 'iscover desc ', 'label' => '设为封面')),
                'defaultOrder' => 'create_time desc'),
        ));
    }

    public function beforeSave() {
        parent::beforeSave();
        foreach (array('create_time', 'update_time') as $key) {
            if (empty($this->$key)) {
                $this->$key = new CDbExpression('NOW()');
            }
        }
        foreach (array('summary', 'content') as $key) {
            if (isset($this->$key)) {
                // $this->$key =   htmlspecialchars($this->$key);
            }
        }
        return true;
    }

}