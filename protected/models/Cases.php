<?php

/**
 * This is the model class for table "cloud_case".
 *
 * The followings are the available columns in table 'cloud_case':
 * @property integer $id
 * @property string $title
 * @property string $summary
 * @property string $imgpath
 * @property string $detail
 * @property integer $isrecommend
 * @property string $product
 * @property string $userexprience
 * @property integer $author
 * @property string $update_time
 * @property string $create_time
 */
class Cases extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Cases the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cloud_case';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('author', 'required'),
            array('isrecommend, author', 'numerical', 'integerOnly' => true),
            array('title,detail,summary', 'match', 'pattern' => '/^[\x{80}-\x{ff}a-zA-Z\d]*$/', 'message' => '请输入正确的{attribute}'),
            array('title', 'length', 'max' => 128, 'message' => '{attribute}长度超出限制'),
            array('summary, imgpath, userexperience', 'length', 'max' => 512, 'message' => '{attribute}长度超出限制'),
            array('imgpath', 'file', 'allowEmpty' => true, 'types' => 'jpg,jpeg, gif, png',
                'maxSize' => 1024 * 1024 * 1, // 1MB
                'tooLarge' => 'The file was larger than 1MB. Please upload a smaller file.', 'message' => '{attribute}只能为图片'),
            array('detail, create_time,product', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, summary, imgpath, detail, isrecommend, product, userexperience, author, update_time, create_time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => '标题',
            'summary' => '摘要',
            'imgpath' => '图片路径',
            'detail' => '详情',
            'isrecommend' => '推荐',
            'product' => '使用产品',
            'userexperience' => '用户感受',
            'author' => '发布者',
            'update_time' => '修改时间',
            'create_time' => '发布时间',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('summary', $this->summary, true);
        $criteria->compare('imgpath', $this->imgpath, true);
        $criteria->compare('detail', $this->detail, true);
        $criteria->compare('isrecommend', $this->isrecommend);
        $criteria->compare('product', $this->product, true);
        $criteria->compare('userexperience', $this->userexperience, true);
        $criteria->compare('author', $this->author);
        $criteria->compare('update_time', $this->update_time, true);
        $criteria->compare('create_time', $this->create_time, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}