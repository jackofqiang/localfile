<?php

/**
 * This is the model class for table "cloud_app".
 *
 * The followings are the available columns in table 'cloud_app':
 * @property integer $id
 * @property string $appname
 * @property string $summary
 * @property string $detail
 * @property string $applogo
 * @property integer $appcategory
 * @property string $buyurl
 * @property string $author
 * @property string $update_time
 * @property string $create_time
 * @property integer $isrecommend
 */
class App extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return App the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cloud_app';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('appcategory, isrecommend', 'numerical', 'integerOnly' => true),
            array('appname, author', 'length', 'max' => 64),
            array('summary, applogo, buyurl', 'length', 'max' => 256),
            array('detail, create_time', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, appname, summary, detail, applogo, appcategory, buyurl, author, update_time, create_time, isrecommend', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    public function scopes() {

        return array(
            'recently' => array(
                'order' => 'create_time DESC',
                'limit' => 10,
            ),
        );
    }

    public function recently($limit = 5) {
        $this->getDbCriteria()->mergeWith(array(
            'order' => 'create_time DESC',
            'limit' => $limit,
        ));
        return $this;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'appname' => 'App名称',
            'summary' => '摘要',
            'detail' => '详情',
            'applogo' => 'App图片',
            'appcategory' => 'App类别',
            'buyurl' => '购买地址',
            'author' => '作者',
            'update_time' => '更新时间',
            'create_time' => '创建时间',
            'isrecommend' => '是否推荐',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('appname', $this->appname, true);
        $criteria->compare('summary', $this->summary, true);
        $criteria->compare('detail', $this->detail, true);
        $criteria->compare('applogo', $this->applogo, true);
        $criteria->compare('appcategory', $this->appcategory);
        $criteria->compare('buyurl', $this->buyurl, true);
        $criteria->compare('author', $this->author, true);
        $criteria->compare('update_time', $this->update_time, true);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('isrecommend', $this->isrecommend);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}