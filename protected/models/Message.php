<?php

/**
 * This is the model class for table "cloud_message".
 *
 * The followings are the available columns in table 'cloud_message':
 * @property string $id
 * @property string $contact
 * @property string $company
 * @property string $industry
 * @property string $demand
 * @property string $mobile
 * @property string $ip
 * @property string $create_time
 * @property integer $status
 */
class Message extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Message the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cloud_message';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('company,contact,mobile', 'required', 'message' => '{attribute}不能为空'),
            array('company,contact,industry', 'match', 'pattern' => '/^[\x{80}-\x{ff}a-zA-Z\d]{2,}$/', 'message' => '请输入正确的{attribute}'),
            array('company,contact,industry', 'match', 'pattern' => '/[a-zA-Z\x{80}-\x{ff}]/', 'message' => '{attribute}不能全部为数字'),
            array('mobile', 'match', 'pattern' => '/^(1[3458]\d{9}|(0\d{2,3}-)?\d{7,8})$/', 'message' => '请输入正确的联系方式'),
            array('status', 'numerical', 'integerOnly' => true),
            array('contact', 'length', 'max' => 32, 'message' => '{attribute}长度超出限制'),
            array('company, industry', 'length', 'max' => 64, 'message' => '{attribute}长度超出限制'),
            array('demand', 'length', 'max' => 512, 'message' => '{attribute}长度超出限制'),
            array('mobile', 'length', 'max' => 12, 'message' => '{attribute}长度超出限制'),
            array('ip', 'length', 'max' => 128, 'message' => '{attribute}长度超出限制'),
            array('create_time', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, contact, company, industry, demand, mobile, ip, create_time, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'contact' => '联系人',
            'company' => '公司名称',
            'industry' => '所属行业',
            'demand' => '需求描述',
            'mobile' => '联系方式',
            'ip' => '用户IP',
            'create_time' => '提交时间',
            'status' => '处理状态',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('contact', $this->contact, true);
        $criteria->compare('company', $this->company, true);
        $criteria->compare('industry', $this->industry, true);
        $criteria->compare('demand', $this->demand, true);
        $criteria->compare('mobile', $this->mobile, true);
        $criteria->compare('ip', $this->ip, true);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeSave() {

        parent::beforeSave();
        foreach (array('demand', 'contact') as $key) {
            if (isset($this->$key)) {
                $this->$key = htmlspecialchars($this->$key);
            }
        }
        return true;
    }
}