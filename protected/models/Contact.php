<?php

/**
 * This is the model class for table "cloud_contact".
 *
 * The followings are the available columns in table 'cloud_contact':
 * @property string $id
 * @property string $username
 * @property string $individual_duties
 * @property string $company
 * @property string $mobile
 * @property string $addr
 * @property string $addr_more
 * @property string $remark
 * @property string $type
 * @property string $ip
 * @property string $create_time
 * @property integer $status
 */
class Contact extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Contact the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cloud_contact';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('company,username,mobile,demandnum,individual_duties,addr_more', 'required', 'message' => '{attribute}不能为空'),
            array('company,username,individual_duties,addr_more', 'match', 'pattern' => '/[\x{00}-\x{ff}]{1,64}/', 'message' => '请输入正确的{attribute}'),
            array('company,username,individual_duties,addr_more', 'match', 'pattern' => '/[a-zA-Z\x{80}-\x{ff}]/', 'message' => '{attribute}不能全部为数字'),
            array('mobile', 'match', 'pattern' => '/^(1[3458]\d{9}|(0\d{2,3}-)?\d{7,8})$/', 'message' => '请输入正确的联系方式'),
            array('status,demandnum', 'numerical', 'integerOnly' => true,'message'=>'{attribute}必须为数字'),
            array('id', 'length', 'max' => 11, 'message' => '{attribute}长度超出限制'),
            array('individual_duties,username,company,type', 'length', 'max' => 32, 'message' => '{attribute}长度超出限制'),
            array('mobile', 'length', 'max' => 12, 'message' => '{attribute}长度超出限制'),
            array('addr, addr_more', 'length', 'max' => 128, 'message' => '{attribute}长度超出限制'),
            array('remark', 'length', 'max' => 512, 'message' => '{attribute}长度超出限制'),
            array('ip', 'length', 'max' => 128, 'message' => '{attribute}长度超出限制'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, username, individual_duties, company, mobile, addr, addr_more, remark, type, ip, create_time,demandnum, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'username' => '姓名',
            'individual_duties' => '个人职务',
            'company' => '公司名称',
            'mobile' => '联系方式',
            'demandnum'=>'需求数量',
            'addr' => '地址',
            'addr_more' => '详细地址',
            'remark' => '备注',
            'type' => '客户类型',
            'ip' => '用户IP',
            'create_time' => '提交时间',
            'status' => '处理状态',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('demandnum', $this->demandnum);
        $criteria->compare('individual_duties', $this->individual_duties, true);
        $criteria->compare('company', $this->company, true);
        $criteria->compare('mobile', $this->mobile, true);
        $criteria->compare('addr', $this->addr, true);
        $criteria->compare('addr_more', $this->addr_more, true);
        $criteria->compare('remark', $this->remark, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('ip', $this->ip, true);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeSave() {
        parent::beforeSave();
        foreach (array('remark', 'username') as $key) {
            if (isset($this->$key)) {
                $this->$key = htmlspecialchars($this->$key);
            }
        }
        return true;
    }

}