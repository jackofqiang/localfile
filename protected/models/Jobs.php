<?php

/**
 * This is the model class for table "cloud_jobs".
 *
 * The followings are the available columns in table 'cloud_jobs':
 * @property integer $id
 * @property integer $num
 * @property string $jobtitle
 * @property string $jobaddr
 * @property string $requirements
 * @property string $responsibilities
 * @property string $post_author
 * @property string $post_time
 */
class Jobs extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Jobs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cloud_jobs';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('post_time', 'required'),
			array('num', 'numerical', 'integerOnly'=>true),
			array('jobtitle, jobaddr, post_author', 'length', 'max'=>64),
			array('responsibilities', 'length', 'max'=>512),
			array('requirements', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, num, jobtitle, jobaddr, requirements, responsibilities, post_author, post_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
                        'num'=>'招聘人数',
			'jobtitle' => '岗位名称',
			'jobaddr' => '工作地点',
			'requirements' => '岗位要求',
			'responsibilities' => '岗位职责',
			'post_author' => '发布人',
			'post_time' => '发布时间',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('num',$this->num);
		$criteria->compare('jobtitle',$this->jobtitle,true);
		$criteria->compare('jobaddr',$this->jobaddr,true);
		$criteria->compare('requirements',$this->requirements,true);
		$criteria->compare('responsibilities',$this->responsibilities,true);
		$criteria->compare('post_author',$this->post_author,true);
		$criteria->compare('post_time',$this->post_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}