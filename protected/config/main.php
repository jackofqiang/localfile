<?php

// uncomment the following to define a path alias
 Yii::setPathOfAlias('bootstrap',dirname(__FILE__).'/../extensions/bootstrap');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => '阳光云 SunCloud',
    'preload' => array('log','bootstrap'),
    'defaultController' => 'index',
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'generatorPaths'=>array('bootstrap.gii'),
            'password' => '123',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
        'api'
    ),
    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'loginUrl' => array('yovoleAdmin/login'),
        ),
        'request' => array(
            'enableCookieValidation' => true,
            'enableCsrfValidation' => true,
        ),
         'bootstrap' => array(
            'class'=>'bootstrap.components.Bootstrap',
        ),
        'mailer' => array(
            'class' => 'ext.mailer.EMailer',
            'pathViews' => 'application.views.email',
            'pathLayouts' => 'application.views.email.layouts'
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'urlSuffix' => '.html',
            'rules' => array(
                'sunapp/?<action:\w*>' => 'plat/<action>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>-<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<page:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<action:contact|jobs>' => 'about/<action>',
            ),
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=suncloud',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',
        ),
        'errorHandler' => array(
            // use 'index/error' action to display errors
            'errorAction' => 'index/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                    'LogPath' => 'log'
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
              array(
              'class'=>'CEmailLogRoute',
              'emails'=>'1291853630.qq.com',
              ),
             * */
            ),
        ),
    ),
    'params' => array(
        'adminEmail' => 'zhaishunqiang@yovole.com',
        'pass' => '109319',
        'smtp' => 'smtp.yovole.com',
        'from' => 'zhaishunqiang@yovole.com',
        'to' => 'zhaishunqiang@yovole.com',
        'uploadImage' => 'upload',
    ),
);