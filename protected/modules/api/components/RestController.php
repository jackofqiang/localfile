<?php

class RestController extends Controller {
 
    /**
     * 发送数据
     * @param array $data 数据
     * @param integer $errorCode 错误代码，默认为 200，即没有错误
     * @param string $errorMessage 错误信息，默认为 null，根据错误代码自动获得信息
     */
    public function send($data, $errorCode = RestError::ERROR_OK, $errorMessage = null) {
        header("Content-type:application/json;");
        $response = array(
            'errorCode' => $errorCode,
            'errorMessage' => $errorMessage == null ? RestError::getErrorMessage($errorCode) : $errorMessage,
            'data' => $data
        );
        echo CJSON::encode($response);
        Yii::app()->end();
    }
 
    /**
     * 发送错误信息
     * @param integer $errorCode 错误代码
     * @param string $errorMessage 错误信息，为 null 时根据错误代码自动获得信息
     */
    public function sendError($errorCode, $errorMessage) {
        header("Content-type:application/json;");
        $response = array(
            'errorCode' => $errorCode,
            'errorMessage' => $errorMessage == null ? RestError::getErrorMessage($errorCode) : $errorMessage,
        );
        echo CJSON::encode($response);
        Yii::app()->end();
    }
 
    /**
     * 动作过滤器，只允许验证通过的用户才可以访问
     * @param CFilterChain $filterChain
     * @throws CHttpException
     */
    public function filterAuthOnly($filterChain) {
        if ($this->validateAuth())
            $filterChain->run();
        else
            throw new CHttpException(401, Yii::t('yii', 'Your authorization is invalid.'));
    }
 
    /**
     * 动作过滤器，只允许通过 PUT 请求的用户才可以访问
     * @param CFilterChain $filterChain
     * @throws CHttpException
     */
    public function filterPutOnly($filterChain) {
        if (Yii::app()->getRequest()->getIsPutRequest())
            $filterChain->run();
        else
            throw new CHttpException(400, Yii::t('yii', 'Your request is invalid.'));
    }
 
    /**
     * 验证用户信息，基于 HTTP Base 验证
     * @return boolean
     */
    protected function validateAuth() {
        $http_authorization = $_SERVER['HTTP_AUTHORIZATION'];
        list($username, $password) = explode(':', base64_decode(substr($http_authorization, 6)));
        $form = new LoginForm();
        $form->username = $username;
        $form->password = $password;
        if ($form->validate()) {
            $form->login();
            return true;
        } else {
            return false;
        }
    }
 
}