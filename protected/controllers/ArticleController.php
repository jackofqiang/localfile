<?php

class ArticleController extends Controller {

    public $layout = "main";

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            'uploadImage' => 'application.controllers.upload.ImageAction',
        );
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'detail', 'uploadImage', 'notice', 'news', 'activity', 'knowledge'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'view', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function beforeAction($action) {
        Yii::app()->getClientScript()->registerScript('menu-click', "
            $('.category li').click(function(){
             $(this).addClass('on').siblings().removeClass('on');
        });
        ", CClientScript::POS_READY);
        
       return  parent::beforeAction($action);
    }
    

    /**
     * Displays the contact page
     */
    public function actionDetail($id) {

        $model = Article::model()->with('enum')->findByPk($id);

        $this->render('detail', array('model' => $model));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $this->layout = "column1";
        $model = new Article;
        $model->update_time = $model->create_time = date('Y-m-d H:i:s');
        $data = $this->listCategory();
        if (isset($_POST['Article'])) {
            $model->attributes = $_POST['Article'];
            $file = CUploadedFile::getInstance($model, 'imgpath');
            if ($file instanceof CUploadedFile) {
                $filetype = $file->getType();
                $filename = $file->getName();
                $uploadfile = "upload/images/" . $filename;
                $model->imgpath = $uploadfile;
                $file->saveAs($uploadfile, true); //上传操作   
            }
            if ($model->save())
                $this->redirect(array('admin'));
        }
        $this->render('create', array(
            'model' => $model,
            'data' => $data
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $this->layout = "column1";
        $model = $this->loadModel($id);
        $model->update_time = date('Y-m-d H:i:s');
        $data = $this->listCategory();
        if (isset($_POST['Article'])) {
            $model->attributes = $_POST['Article'];
            if (empty($model->update_time)) {
                $model->update_time = new CDbExpression('NOW()');
            }
            if ($model->save())
                $this->redirect(array('admin'));
        }
        $this->render('update', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $criteria = new CDbCriteria();
        $criteria->order = ' isrecommend DESC,create_time DESC ';
        $criteria->condition = 't.category = 6 ';
        $count = Article::model()->count($criteria);

        $pager = new CPagination($count);
        $pager->pageSize = 10;
        $pager->applyLimit($criteria);

        $articles = Article::model()->with('enum')->findAll($criteria);

        $this->render('index', array('pages' => $pager, 'articles' => $articles));
    }

    public function actionNotice() {
        $criteria = new CDbCriteria();
        $criteria->order = ' isrecommend DESC,create_time DESC ';
        $criteria->condition = 't.category = 6 ';
        $count = Article::model()->count($criteria);

        $pager = new CPagination($count);
        $pager->pageSize = 10;
        $pager->applyLimit($criteria);

        $articles = Article::model()->with('enum')->findAll($criteria);

        $this->render('notice', array('pages' => $pager, 'articles' => $articles));
    }

    public function actionNews() {
        $criteria = new CDbCriteria();
        $criteria->order = ' isrecommend DESC,create_time DESC ';
        $criteria->condition = 't.category = 7 ';
        $count = Article::model()->count($criteria);

        $pager = new CPagination($count);
        $pager->pageSize = 10;
        $pager->applyLimit($criteria);

        $articles = Article::model()->with('enum')->findAll($criteria);

        $this->render('news', array('pages' => $pager, 'articles' => $articles));
    }

    public function actionKnowledge() {
        $criteria = new CDbCriteria();
        $criteria->order = ' isrecommend DESC,create_time DESC ';
        $criteria->condition = 't.category = 8 ';
        $count = Article::model()->count($criteria);

        $pager = new CPagination($count);
        $pager->pageSize = 10;
        $pager->applyLimit($criteria);
        $articles = Article::model()->with('enum')->findAll($criteria);
        $this->render('knowledge', array('pages' => $pager, 'articles' => $articles));
    }

    public function actionActivity() {
        $criteria = new CDbCriteria();
        $criteria->order = ' isrecommend DESC,create_time DESC ';
        $criteria->condition = 't.category = 9 ';
        $count = Article::model()->count($criteria);

        $pager = new CPagination($count);
        $pager->pageSize = 10;
        $pager->applyLimit($criteria);
        $articles = Article::model()->with('enum')->findAll($criteria);
        $this->render('activity', array('pages' => $pager, 'articles' => $articles));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $this->layout = "column1";
        $model = new Article('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Article'])){
            $model->attributes = $_GET['Article'];
        }
        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Article the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Article::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Article $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'article-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function listCategory() {

        $data = Enum::model()->findAllByAttributes(array('category' => 'article'));
        return CHtml::listData($data, 'id', 'description');
    }


}

?>