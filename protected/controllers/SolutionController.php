<?php

class SolutionController extends Controller {

    public $layout = "main";
    public $menuIndex = 3;

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $model = new Message();
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'Message-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['Message'])) {
            $model->attributes = $_POST['Message'];
            $model->ip = Yii::app()->request->userHostAddress;
            $success = false;
            if ($model->validate() && $model->save()) {
                $model->unsetAttributes();
                $success = true;
                echo json_encode(array('message' => '已提交，我们会尽快联系您', 'status' => '1'));
            } else {
                echo json_encode(array('message' => '提交失败', 'status' => '0'));
            }
            if ($success) {
                $message = "用户提交信息简介：" . "<br/>";
                foreach ($_POST['Message'] as $key => $value) {
                    $message.=$model->getAttributeLabel($key) . ":" . $value . "<br/>";
                }
                $subject = "云方案用户提交信息";
                $this->sendEmail($subject, $message);
            }
            if (Yii::app()->request->isAjaxRequest) {
                Yii::app()->end();
            }
        }
        $this->render('index', array('model' => $model));
    }

}
