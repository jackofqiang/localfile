<?php

class PlatController extends Controller {

    public $layout = "main";
    public $menuIndex=2;

    /*     * l
     * Declares class-based actions.
     */

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndexbak() {

        $criteria = new CDbCriteria();

        $appcategory = 1;
        $categories = array('1', '2', '3', '4', '5');
        if (isset($_POST['appcategory']) && in_array($_POST['appcategory'], $categories)) {
            $appcategory = $_POST['appcategory'];
        }
        $criteria->condition = 'appcategory =' . $appcategory;
        $count = App::model()->count($criteria);
        $criteria->order = 'create_time desc';
        $pager = new CPagination($count);
        $pager->pageSize = 10;
        $pager->applyLimit($criteria);

        $apps = App::model()->findAll($criteria);
        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('_index', array(
                'apps' => $apps, 'pages' => $pager,
            ));
            Yii::app()->end();
        }
        $this->render('index', array('pages' => $pager, 'apps' => $apps));
    }
    
     public function actionIndex() {

        
        $this->render('static');
    }

}