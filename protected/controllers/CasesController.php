<?php

class CasesController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';
    public $menuIndex = 5;

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'view'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $this->layout = "column1";
        $model = new Cases;
        $model->update_time = $model->create_time = new CDbExpression('NOW()');
        if (isset($_POST['Cases'])) {

            if (is_array($_POST['Cases']['product']))
                $_POST['Cases']['product'] = implode(",", $_POST['Cases']['product']);
            else
                $_POST['Cases']['product'] = '';
            $model->attributes = $_POST['Cases'];
            $file = CUploadedFile::getInstance($model, 'imgpath');
            if ($file instanceof CUploadedFile) {
                $filetype = $file->getType();
                $filename = $file->getName();
                $uploadfile = "upload/images/" . $filename;
                $model->imgpath = $uploadfile;
                $file->saveAs($uploadfile, true); //上传操作   
            }
            $model->update_time = $model->create_time = new CDbExpression('NOW()');
            $model->author = 1;
            if ($model->save())
                $this->redirect(array('admin'));
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $this->layout = "column1";
        $model = $this->loadModel($id);
        $model->product = explode(',', $model->product);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Cases'])) {
            $oldimgpath = $model->imgpath;
            if (is_array($_POST['Cases']['product']))
                $_POST['Cases']['product'] = implode(",", $_POST['Cases']['product']);
            else
                $_POST['Cases']['product'] = '';
            $model->attributes = $_POST['Cases'];
            $file = CUploadedFile::getInstance($model, 'imgpath');
            if (is_object($file)) {
                $filetype = $file->getType();
                $filename = $file->getName();
                $uploadfile = "upload/images/" . $filename;
                $file->saveAs($uploadfile, true); //上传操作   
                $model->imgpath = $uploadfile;
            } else {
                $model->imgpath = $oldimgpath;
            }
            if (empty($model->update_time)) {
                $model->update_time = new CDbExpression('NOW()');
            }
            if ($model->save())
                $this->redirect(array('admin'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        //$dataProvider=new CActiveDataProvider('Cases');
        $criteria = new CDbCriteria();

        $count = Cases::model()->count($criteria);
        $criteria->order = 'create_time asc';

        $pager = new CPagination($count);
        $pager->pageSize = 4;
        $pager->applyLimit($criteria);

        $articles = Cases::model()->findAll($criteria);
        $this->render('index', array('pages' => $pager, 'articles' => $articles));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $this->layout = "column1";
        $model = new Cases('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Cases']))
            $model->attributes = $_GET['Cases'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Cases the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Cases::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Cases $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'cases-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
