<?php

class IndexController extends Controller {

    public $layout = "main";
    
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $conditions="category=:category";
        $notices = Article::model()->recently(5)->findAll($conditions,array(':category'=>6));
        $news = Article::model()->recently(5)->findAll($conditions,array(':category'=>7));
        $knowledge = Article::model()->recently(5)->findAll($conditions,array(':category'=>8));
        $activity = Article::model()->recently(5)->findAll($conditions,array(':category'=>9));
        $attributes=array('iscover'=>1);
        $covers = Article::model()->findAllByAttributes($attributes);
        $this->render('index', array('notices' => $notices,'notices' => $notices,'news' => $news,'knowledge' => $knowledge,'activity'=>$activity,'covers'=>$covers));
    }

    public function actionSitemap() {

        $this->render('sitemap');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->renderPartial('404', $error);
        }
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

}
