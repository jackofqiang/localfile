<?php

class DesktopController extends Controller {

    public $layout = "main";
    public $menuIndex = 4;

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $model = new Contact();
        $this->render('index', array("model" => $model));
    }

    /**
     * 弹出层显示
     */
    public function actionPopup() {
        $model = new Contact();
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'Contact-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Contact'])) {
            $success = false;
            $model->attributes = $_POST['Contact'];
            $model->ip = Yii::app()->request->userHostAddress;
            if ($model->save()) {
                $model->unsetAttributes();
                $success = true;
                $this->renderPartial("_success", array(), false, true);
            } else {
                $this->renderPartial("_form", array("model" => $model), false, true);
            }
            if ($success) {
                $message = "用户提交信息简介：" . "<br/>";
                foreach ($_POST['Contact'] as $key => $value) {
                    $message.=$model->getAttributeLabel($key) . ":" . $value . "<br/>";
                }
                $subject = "云桌面用户提交信息";
                $this->sendEmail($subject, $message);
            }
            if (Yii::app()->request->isAjaxRequest) {
                Yii::app()->end();
            }
        } else {
            $this->renderPartial('_form', array("model" => $model), false, true);
        }
    }

    /*
     * 弹出层数据提交
     */
    public function actionDialog() {

        $model = new Contact();
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'Contact-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Contact'])) {
            $success = false;
            $_POST['Contact']['addr']=$_POST['province'].' '.$_POST['city'];
            $model->attributes = $_POST['Contact'];
            $model->ip = Yii::app()->request->userHostAddress;
            if ($model->save()) {
                $model->unsetAttributes();
                $success = true;
                echo json_encode(array('message' => '已提交，我们会尽快联系您', 'status' => '1'));
            } else {
                echo json_encode(array('message' => '提交失败', 'status' => '0'));
            }
            if ($success) {
                $message = "用户提交信息简介：" . "<br/>";
                foreach ($_POST['Contact'] as $key => $value) {
                    $message.=$model->getAttributeLabel($key) . ":" . $value . "<br/>";
                }
                $subject = "云桌面用户提交信息";
                $this->sendEmail($subject, $message);
            }
            if (Yii::app()->request->isAjaxRequest) {
                Yii::app()->end();
            }
        }
    }

}

?>