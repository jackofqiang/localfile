<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();
    public $menuIndex = 1;
    protected $user = null;
    protected $cs=null;
    protected $baseUrl='';
    protected $gameHost="http://www.suncloud.cn";
//    public function __construct($id, $module = null) {
//
//        parent::__construct($id, $module);
//        $app=Yii::app();
//        $this->user = $app->user;
//        $this->baseUrl = Yii::app()->BaseUrl;
//    }
    
    public function init() {
        parent::init();
        $app=Yii::app();
        $this->user = $app->user;
        $this->cs=$app->getClientScript();
        $this->baseUrl = Yii::app()->BaseUrl;
    }

    public function beforeAction($action) {
        // header("Location:maintain.html");
        return true;
    }

    //发邮件
    public function sendEmail($subject, $body = NULL) {
        $password = Yii::app()->params['pass'];
        $smtp = Yii::app()->params['smtp'];
        $from = Yii::app()->params['from'];
        $to = Yii::app()->params['to'];
        $mail = Yii::app()->mailer;
        $mail->Host = $smtp;
        $mail->IsSMTP();
        $mail->Username = $from;
        $mail->Password = $password;
        $mail->SMTPAuth = true;
        $mail->IsHTML(true);
        $mail->CharSet = "utf-8";
        $mail->From = $from;
        $mail->FromName = 'qiang';
        $mail->AddReplyTo($from);
        $mail->AddAddress($to);
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->Send();
    }

}