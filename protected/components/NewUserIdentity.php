<?php
class NewUserIdentity extends CUserIdentity
{
    /**
    * User's attributes
    * @var array
    */
    public $user;
 
    public function authenticate()
    {
        $this->errorCode=self::ERROR_PASSWORD_INVALID;
        $user=Admin::model()->findByAttributes(array('username'=>CHtml::encode($this->username)));
 
        if ($user)
        {
            if ($user->password === md5($this->password)) {
                $this->errorCode=self::ERROR_NONE;
                $this->setUser($user);
            }
        }
 
        unset($user);
        return !$this->errorCode;
    }
 
    public function getUser()
    {
        return $this->user;
    }
 
    public function setUser(CActiveRecord $user)
    {
        $this->user=$user->attributes;
    }
}
?>