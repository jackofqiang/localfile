<?php
/* @var $this AboutController */
/* @var $model About */
/* @var $form CActiveForm */
?>
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'about-form',
        'enableAjaxValidation' => false,
    ));
    ?>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title', array('size' => 32, 'maxlength' => 32)); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'summary'); ?>
        <?php echo $form->textField($model, 'summary', array('size' => 60, 'maxlength' => 128)); ?>
        <?php echo $form->error($model, 'summary'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'detail'); ?>
        <?php $this->widget('ext.ckeditor.CKEditor', array('model' => $model, 'attribute' => 'detail', 'language' => 'zh_cn', 'editorTemplate' => 'full',)); ?>
        <?php echo $form->error($model, 'detail'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'isrecommend', array('style' => 'display:inline;')); ?>
        <?php echo $form->radioButtonList($model, 'isrecommend', array('否', '是'), array('separator' => '&nbsp;', 'labelOptions' => array('style' => 'display:inline;'))); ?>
        <?php echo $form->error($model, 'isrecommend'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'create_time'); ?>
        <?php
        $this->widget('ext.timepicker.timepicker', array(
            'model' => $model,
            'name' => 'create_time',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth' => true,
                'changeYear' => true,
                'yearRange' => '-30:+30'
            ),
        ));
        ?> 
        <?php echo $form->error($model, 'create_time'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'update_time'); ?>
        <?php
        $this->widget('ext.timepicker.timepicker', array(
            'model' => $model,
            'name' => 'update_time',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth' => true,
                'changeYear' => true,
                'yearRange' => '-30:+30'
            ),
        ));
        ?> 
        <?php echo $form->error($model, 'update_time'); ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->