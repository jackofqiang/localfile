<?php
/* @var $this AboutController */

$this->pageTitle = Yii::app()->name . "--关于我们";
?>
<div class="banner ov">
    <div class="w1000"><?php echo CHtml::image($this->baseUrl . "/images/banner-aboutus.gif", "picture"); ?></div>
</div>
<div class="w1000 ov">
    <div class="sidebar mb13 ov z">
        <div class="sidebar-nav mb13 ov">
            <h1>关于我们</h1>
            <ul class="menu">
                <li class="on"><a href="<?php echo $this->createUrl('about/'); ?>">公司介绍</a></li>
                <li><a href="<?php echo $this->createUrl('contact/'); ?>">联系我们</a></li>
                <li><a href="<?php echo $this->createUrl('jobs/'); ?>">招贤纳士</a></li>
            </ul>
        </div>
    </div>
    <div class="aboutus-right ov y" style="min-height:300px;">
        <?php if (is_object($model)) { ?>
            <h2><?php echo $model->title; ?></h2>
            <?php echo $model->detail; ?>
        <?php } else { ?>
            <h2>阳光云</h2>
            <p>阳光云(www.suncloud.cn)是上海有孚计算机网络有限公司精心打造的互联网服务品牌，有孚网络依托其雄厚的技术研发实力，近十年的IDC成功运营经验及前瞻性的战略布局，与微软、IBM、中国电信、中国移动等全球500强企业达成了战略合作伙伴关系。在短短五年时间里，实现了以上海为中心、渠道覆盖全国、业务辐射全球的立体化营销网络。阳光云是一个基于WEB服务的弹性云技术Iaas平台。将计算、存储与网络等相关资源进行整合，形成资源池，可以让用户按需取用，使其资源利用率最大化，减少投入成本，致力于为企业提供最专业的云计算服务。具有高性能、高伸缩性、高可用性、高可视性、高可靠性、高安全性等显著特点。目前提供云平台、云方案、云桌面等相关服务。专业化的服务模式，优越式的技术安全保障有足以成为您选择的理由。</p>
            <p>上海有孚计算机网络有限公司创立于2001年，是国内领先的创新型高科技云计算服务商。旗下有“阳光互联”品牌。公司以数据中心（IDC）整机房租用、服务器租用托管服务、基础服务（Iaas）、软件服务（Saas）等为主营业务。</p>
            <p>上海有孚计算机网络有限公司已成功打造知名的服务品牌"阳光互联"，通过云计算方式向企业提供低成本高性能的平台式信息化服务。在融入云计算概念基础上，通过与微软、IBM、中国电信、中国联通等大型服务商、运营商的合作，为用户提供一站式的信息化服务。上海有孚计算机网络有限公司已成为中国首批最大的云计算服务商之一，是微软全球500强企业战略合作伙伴。</p>
        <?php } ?>
    </div>
</div>
