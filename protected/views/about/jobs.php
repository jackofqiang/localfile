<?php
/* @var $this AboutController */

$this->pageTitle = Yii::app()->name . "--招贤纳士";
?>
<div class="banner ov">
    <div class="w1000"><?php echo CHtml::image(Yii::app()->BaseUrl . "/images/banner-aboutus.gif", "关于我们"); ?></div>
</div>
<div class="w1000 ov">
    <div class="sidebar mb13 ov z">
        <div class="sidebar-nav mb13 ov">
            <h1>关于我们</h1>
            <ul class="menu">
                <li><a href="<?php echo $this->createUrl('about/'); ?>">公司介绍</a></li>
                <li><a href="<?php echo $this->createUrl('contact/'); ?>">联系我们</a></li>
                <li class="on"><a href="<?php echo $this->createUrl('jobs/'); ?>">招贤纳士</a></li>
            </ul>
        </div>
    </div>
    <div class="aboutus-right ov y" style="min-height:300px;" id="jobs">
        <h2>招贤纳士</h2>
        <p>我们所遵循的企业理念是：“为社会提供有价值的、卓越的产品或服务；为员工提供广阔的发展空间、改善生活质量；员工与 员工、工序与工序、部门与部门之间的责任体现是认真负责的工作质量和把困难留给自己，把方便让给别人。 员工和企业利益共享，共同发展”。</p>
        <table width="705px" class="box ov" cellpadding="0" cellspacing="0">
            <tr>
                <th>更新日期</th>
                <th>岗位名称</th>
                <th>招聘人数</th>
                <th>工作地点</th>
                <th>详情描述</th>
            </tr>
            <?php
            if (isset($jobs)) {
                foreach ($jobs as $job) {
                    ?>
                    <tr>
                        <td><?php
                            //list($date, ) = explode(" ", $job->post_time);
                            echo date('Y-m-d');
                            ?></td>
                        <td><?php echo $job->jobtitle; ?></td>
                        <td><?php echo $job->num == 0 ? "若干" : $job->num; ?></td>
                        <td><?php echo $job->jobaddr; ?></td>
                        <td id="toggle" class="c369 toggle" >查看详情</td>
                    </tr>
                    <tr>
                        <td  colspan="5" id="content" style=" margin:5px 0; overflow:hidden;display:none;">
                            <div class="cont" >
                                <div class="msg-a">
                                    <div class="">
                                        <?php
                                        $r = $job->requirements;
                                        echo!empty($r) ? $r : "暂无内容";
                                        ?>
                                    </div>
                                </div>
                                <a class="bt-apply" title="立即申请" href="mailto:hongmengchen@yovole.com"></a>

                            </div>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </table>
    </div>
</div>
<script type="text/javascript" >
    $(function() {
        $(".toggle").click(function() {
            $(this).parent().next().children().toggle();

        });
    });

</script>