<?php
/* @var $this AboutController */

$this->pageTitle = Yii::app()->name . "--联系我们";
?>
<div class="banner ov">
    <div class="w1000"><?php echo  CHtml::image(Yii::app()->BaseUrl."/images/banner-aboutus.gif","关于我们");?></div>
</div>

<div class="w1000 ov">
    <div class="sidebar mb13 ov z">
        <div class="sidebar-nav mb13 ov">
            <h1>关于我们</h1>

            <ul class="menu">
                <li ><a href="<?php echo $this->createUrl('about/');?>">公司介绍</a></li>
                <li class="on"><a href="<?php echo $this->createUrl('contact/');?>">联系我们</a></li>
                <li><a href="<?php echo $this->createUrl('jobs/');?>">招贤纳士</a></li>
            </ul>
        </div>

    </div>

    <div class="aboutus-right ov y" style="min-height:300px;" id="contact">
        <h2>联系我们</h2>
        <div class="map"><?php echo  CHtml::image(Yii::app()->BaseUrl."/images/img-map.jpg","map");?></div>
        <div class="tel"><strong class="c369">Telephone</strong><div>全国统一24小时服务热线 : <span class="c369">400-720-0020</span></div></div>
        <div class="adress"><strong class="c369">Address</strong>
            <div>总公司 : 上海有孚计算机网络有限公司 上海市杨浦区国定路323号12楼 邮编：200433 <br />
                <span class="c369">Tel :+86-021-51263688</span><br />
                <span class="c369">Fax :+86-021-51263588</span><br /><br/>

                成都分公司 : 地址：成都市红星路三段16号正熙国际大厦1904 邮编：610016 <br />
                <span class="c369">Tel1 :+86-028-86650708 Tel2 :+86-028-86657208 Fax :+86-028-86650969</span><br /><br />

                北京分公司 : 地址：北京市东城区东直门外大街42号宇飞大厦1119室 邮编：100027<br />
                <span class="c369">Tel :+86-010-57799018 Fax :+86-010-64171150 </span><br /><br />

                深圳分公司 : 地址：深圳市-福田区-泰然七路25号苍松大厦（北座）1105室 邮编：518000<br />
                <span class="c369">Tel :+86-0755-83254233  Fax :+86-0755-83258119</span><br /><br />
            </div>
        </div>
    </div>
</div>
