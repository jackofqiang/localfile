<style type="text/css">
    .ui-dialog-titlebar {
        display: none;
    }
    .ui-widget-content{
        border:none;
        background:none;
    }
    .ui-dialog{
        padding:0px;
    }
    .ui-dialog .ui-widget-content{
        padding:0px;
    }
    #mydialog{
        overflow:hidden;
    }

</style>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'mydialog',
    'options' => array(
        'title' => '',
        'modal' => true,
        'autoOpen' => false,
        'height' => '623',
        'width' => '1000',
        'resizable' => false,
        'close'=>'js:function(){alert(123);window.frames[0].location.reload();}',
    ),
));
?>
    <iframe src="<?php echo $this->createUrl('desktop/popup');?>" style="border:none;height:623px;width:1000px;" scrolling="no" ></iframe>
<?php
$this->endWidget();
?>