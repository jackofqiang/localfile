<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'mydialog',
    'options' => array(
        'title' => '',
        'modal' => true,
        'autoOpen' => false,
        'height' => 'auto',
        'width' => '1000',
        'resizable' => false,
        'close'=>'js:function(){$("form").find("input,textarea").not("[name=\'YII_CSRF_TOKEN\']").add("select").val("");
                                $("form").find("#Contact_demandnum").val("50");
                                $(".errorMessage").hide().html("");
                                $form.resetForm();
                                $form.resetStatus();
                                $(".Validform_error").removeClass("Validform_error");
                                $(".Validform_checktip").removeClass("Validform_wrong").removeClass("Validform_right").html("");
                                $("div.trial:eq(1)").show().prev().hide();
                                }'
    ),
));
?>

<div class="win02 w990 ov">
    <div class="win02_toph"><h1>免费试用</h1><a href="javascript:void(0);" class="close y"> </a></div>
    <div class="trial" style="text-align:center;display:none;">
        <?php echo CHtml::image(Yii::app()->BaseUrl . "/images/img-trail-suc.gif", "trial-suc", array('height' => 89, 'width' => 228, 'style' => 'margin-top:140px;')); ?>  
        <p style="font-size:16px;margin: auto;">我们会尽快联系您 !  <span style="font-size:24px;color:#31bcb7;margin:0 10px;" id="counter-clock">3</span> 秒后关闭···</p>
    </div>
    <div class="trial">
        <?php echo CHtml::image(Yii::app()->BaseUrl . "/images/winbg-trial.gif", "trial", array('class' => 'z', 'style' => 'margin:50px 0 0 60px;')); ?>  
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'Contact-form',
            'enableAjaxValidation' => false,
            'enableClientValidation'=>false,
            'action' => array('dialog'),
            'htmlOptions' => array(
                'class' => 'y',
            ))
        );
        ?>
        <p class="info">请留下您的联系方式，我们会尽快安排工作人员与您联系</p>

        <dl>
            <dt>单位名称<font> * </font>：</dt>
            <dd><?php echo $form->textField($model, 'company', array('class' => 'int','datatype'=>'s2-18','nullmsg'=>'单位名称不能为空','errormsg'=>'请输入正确的单位名称')); ?>
                <span class="txt" style="color:#F00"><?php echo $form->error($model, 'company'); ?></span>
            </dd>
        </dl>
        <dl>
            <dt>个人职务<font> * </font>：</dt>
            <dd><?php echo $form->textField($model, 'individual_duties', array('class' => 'int','datatype'=>'s2-18','nullmsg'=>'个人职务不能为空','errormsg'=>'请输入正确的个人职务')); ?>
                <span class="txt" style="color:#F00"><?php echo $form->error($model, 'individual_duties'); ?></span>
            </dd>
        </dl>
        <dl>
            <dt>联系人姓名<font> * </font>：</dt>
            <dd><?php echo $form->textField($model, 'username', array('class' => 'int','datatype'=>'s2-18','nullmsg'=>'姓名不能为空','errormsg'=>'请输入正确的姓名')); ?>
                <span class="txt" style="color:#F00"><?php echo $form->error($model, 'username'); ?></span>
            </dd>
        </dl>
        <dl>
            <dt>联系电话<font> * </font>：</dt>
            <dd><?php echo $form->textField($model, 'mobile', array('class' => 'int','datatype'=>'phone','nullmsg'=>'联系方式不能为空','errormsg'=>'请输入正确的联系方式')); ?>
                <span class="txt" style="color:#F00"><?php echo $form->error($model, 'mobile'); ?></span>
            </dd>
        </dl>
        <dl>
            <dt>地址<font> * </font>：</dt>
            <dd>
                <select name="province" datatype="address" nullmsg="请选择省份"></select>
                <select name="city" datatype="address" nullmsg="请选择城市"></select><br/>
                <span class="Validform_checktip"></span>
                <?php echo $form->error($model, 'addr', array('style' => 'margin-top:3px;')); ?>
            </dd>
        </dl>
        <dl>
            <dt>详细地址<font> * </font>：</dt>
            <dd><?php echo $form->textField($model, 'addr_more', array('class' => 'int', 'style' => 'width:310px;','datatype'=>'s2-18','nullmsg'=>'详细地址不能为空','errormsg'=>'请输入正确的地址')); ?>
                <?php echo $form->error($model, 'addr_more', array('style' => 'margin-top:3px;')); ?><br/>
            </dd>
        </dl>
        <dl>
            <dt>需求数量<font> * </font>：</dt>
            <dd><?php echo $form->textField($model, 'demandnum', array('class' => 'int','datatype'=>'n','nullmsg'=>'数量不能为空','errormsg'=>'请输入数字，如50')); ?>
                <span class="txt">台</span>
                <span class="txt" style="color:#F00"> <?php echo $form->error($model, 'demandnum'); ?></span>
            </dd>
        </dl>
        <dl>
            <dt>备注：</dt>
            <dd><?php echo $form->textArea($model, 'remark'); ?></dd>
        </dl>
        <div class="ov" style="margin:20px auto; text-align:center">
            <?php
//            echo CHtml::ajaxSubmitButton('', CHtml::normalizeUrl(array('dialog')), array(
//                'success' => 'function(data){
//                        if(data.status=="1")
//                        {
//                            $("#yt0").attr("disabled",false);$("div.trial:first").show().next().hide();
//                            $("#counter-clock").counter();
//                        }else{
//                            $("form :submit").attr("disabled",false);
//                         }
//                        }',
//                'dataType' => 'json'), array("class" => "sub", 'onclick' => '$(this).attr("disabled",true);', 'style' => 'cursor:pointer;')
//            );
            echo CHtml::submitButton('',array('class'=>'sub','style'=>'cursor:pointer'));
            ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
<?php
$this->endWidget();
?>
<?php $this->cs->registerScriptFile($this->baseUrl . "/js/validform.js",CClientScript::POS_END); ?>
<?php $this->cs->registerCssFile($this->baseUrl . "/css/win.css"); ?>
<?php $this->cs->registerCssFile($this->baseUrl . "/css/validform.css"); ?>
<?php $this->cs->registerScriptFile($this->baseUrl . "/js/counter.js",CClientScript::POS_END); ?>
<?php $this->cs->registerScript("close-dialog", '$("a.close").click(function(){$("#mydialog").dialog("close");});', CClientScript::POS_READY); ?>
<?php $this->cs->registerScriptFile($this->baseUrl . "/js/city.js",CClientScript::POS_END); ?>
<?php $this->cs->registerScript("select-city", "hw_init('province','city','北京','北京');
$('[name=province]').change(function(){ hw_select('province','city');
});", CClientScript::POS_READY); ?>
<?php $this->cs->registerScript("validform", '$form=$("#Contact-form").Validform({
//    tiptype:3,
    tiptype:function(msg,o,cssctl){
        if(!o.obj.is("form")){
            if(o.obj.siblings(".Validform_checktip").length==0){
               o.obj.parent().append("<span class=\'Validform_checktip\' />");
               o.obj.parent().next().find(".Validform_checktip").remove();
             }
            
            var objtip=o.obj.siblings(".Validform_checktip");
            cssctl(objtip,o.type);
            objtip.text(msg);
        }else{
            var objtip=o.obj.find("#msgdemo");
            if(objtip){
                cssctl(objtip,o.type);
                objtip.text(msg);
            }
        }
    },
    showAllError:true,
    postonce:true,
    ajaxPost:true,
    datatype:{
       address:function(gets,obj,curform,regex){
        var province=$("select[name=\'province\']").val()=="",city=$("select[name=city]").val()=="";
        if(province&&city){ return "请选择省份和城市";}
        if(province){ return "请选择省份";}
        if(city){return "请选择城市";}
        return  true;
       },
       phone:/^(1[3458]\d{9}|(0\d{2,3}-)?\d{7,8})$/,
    },
    callback:function(data){
        if(data.status=="1")
        {
            $("div.trial:first").show().next().hide();
            $("#counter-clock").counter();
        }else if(data.status=="0")
        {
            
        }
    }
    });', CClientScript::POS_READY); ?>
