<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>免费适用申请</title>
    </head>
    <body>
        <div class="win02 w990">
            <div class="win02_toph"><h1>免费试用</h1><a href="javascript:void(0);" class="close y"> </a></div>
            <div class="trial" style="text-align:center;">
                <?php echo CHtml::image(Yii::app()->BaseUrl . "/images/img-trail-suc.gif", "trial-suc", array('height' => 89, 'width' => 228, 'style' => 'margin-top:140px;')); ?>  
                <p style="font-size:16px;margin: auto;">我们会尽快联系您 !  <span style="font-size:24px;color:#31bcb7;margin:0 10px;" id="counter-clock">3</span> 秒后关闭···</p>
            </div>
        </div>
        <?php Yii::app()->getClientScript()->registerCssFile($this->baseUrl . "/css/win.css"); ?>
        <?php Yii::app()->getClientScript()->registerScriptFile($this->baseUrl . "/js/counter.js",CClientScript::POS_END); ?>
        <?php Yii::app()->getClientScript()->registerScript("close-dialog", '$("a.close").click(function(){parent.$("#mydialog").dialog("close");});', CClientScript::POS_READY); ?>
        <?php Yii::app()->getClientScript()->registerScript("counter-clock", '$("#counter-clock").counter({context:parent});', CClientScript::POS_READY); ?>
    </body>
</html>
