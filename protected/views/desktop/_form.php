<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>免费适用申请</title>
    </head>
    <body>
        <div class="win02 w990 ov">
            <div class="win02_toph"><h1>免费试用</h1><a href="javascript:void(0);" class="close y"> </a></div>
            <div class="trial">
                <?php echo CHtml::image(Yii::app()->BaseUrl . "/images/winbg-trial.gif", "trial", array('class' => 'z', 'style' => 'margin:50px 0 0 60px;')); ?>  
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'Contact-form',
                    'enableAjaxValidation' => true,
                    'action' => array('popup'),
                    'htmlOptions' => array(
                        'class' => 'y',
                    ))
                );
                ?>
                <p class="info">请留下您的联系方式，我们会尽快安排工作人员与您联系</p>

                <dl>
                    <dt>单位名称<font> * </font>：</dt>
                    <dd><?php echo $form->textField($model, 'company', array('class' => 'int')); ?>
                        <span class="txt" style="color:#F00"><?php echo $form->error($model, 'company'); ?></span>
                    </dd>
                </dl>
                <dl>
                    <dt>个人职务<font> * </font>：</dt>
                    <dd><?php echo $form->textField($model, 'individual_duties', array('class' => 'int')); ?>
                        <span class="txt" style="color:#F00"><?php echo $form->error($model, 'individual_duties'); ?></span>
                    </dd>
                </dl>
                <dl>
                    <dt>联系人姓名<font> * </font>：</dt>
                    <dd><?php echo $form->textField($model, 'username', array('class' => 'int')); ?>
                        <span class="txt" style="color:#F00"><?php echo $form->error($model, 'username'); ?></span>
                    </dd>
                </dl>
                <dl>
                    <dt>联系电话<font> * </font>：</dt>
                    <dd><?php echo $form->textField($model, 'mobile', array('class' => 'int')); ?>
                        <span class="txt" style="color:#F00"><?php echo $form->error($model, 'mobile'); ?></span>
                    </dd>
                </dl>
                <dl>
                    <dt>地址<font> * </font>：</dt>
                    <dd><?php echo $form->textField($model, 'addr_more', array('class' => 'int', 'style' => 'width:310px;')); ?>
                        <span class="txt" style="color:#F00"><?php echo $form->error($model, 'addr_more', array('style' => 'margin-top:3px;')); ?></span>
                    </dd>
                </dl>
                <dl>
                    <dt>需求数量<font> * </font>：</dt>
                    <dd><?php echo $form->textField($model, 'demandnum', array('class' => 'int')); ?>
                        <span class="txt">台</span>
                        <span class="txt" style="color:#F00"> <?php echo $form->error($model, 'demandnum'); ?></span>
                    </dd>
                </dl>
                <dl>
                    <dt>备注：</dt>
                    <dd><?php echo $form->textArea($model, 'remark'); ?></dd>
                </dl>
                <div class="ov" style="margin:20px auto; text-align:center">
                    <?php
                    echo CHtml::submitButton('', array("class" => "sub", 'style' => 'cursor:pointer;'));
                    ?>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
        <?php Yii::app()->getClientScript()->registerCssFile($this->baseUrl . "/css/win.css"); ?>
        <?php Yii::app()->getClientScript()->registerScript("close-dialog", '$("a.close").click(function(){parent.$("#mydialog").dialog("close");});', CClientScript::POS_READY); ?>
    </body>
</html>