<?php
$this->pageTitle = Yii::app()->name . "--云桌面";
?>
<style type="text/css">
    .ui-dialog-titlebar {
        display: none;
    }
    .ui-widget-content{
        border:none;
        background:none;
    }
    .ui-dialog{
        padding:0px;
    }
    .ui-dialog .ui-widget-content{
        padding:0px;
    }
    #mydialog{
        overflow:hidden;
    }

</style>
<div class="banner ov">
    <div class="bnt"></div>
    <div class="w1000" style="position:relative;">
        <div class="bnerbt">
            <span style="line-height:42px; float:left;">咨询电话：18616882198</span>
            <?php
            // the link that may open the dialog
            echo CHtml::link('免费试用', 'javascript:void(0);', array(
                'onclick' => '$("#mydialog").dialog("open"); return false;',
            ));
            ?>
        </div>
        <?php echo CHtml::image(Yii::app()->BaseUrl . "/images/banner-desk.jpg", "banner", array('height' => 173, 'width' => 1000)); ?>
    </div>   
</div>

<div class="deskbox01 ov">
    <div class="w1000 ov">
        <?php echo CHtml::image(Yii::app()->BaseUrl . "/images/img-desk1.gif", "logo", array('height' => 335, 'width' => 464, 'class' => 'z', 'style' => 'margin-top:5px;')); ?>
        <div class="msg y">
            <h1>阳光云桌面</h1><h2>—— 基于云计算技术提供的虚拟办公桌面</h2>
            <p>阳光云桌面是由上海有孚计算机网络有限公司自主研发，是目前中国唯一具备即付即用、按需付费能力的云桌面基础服务。</p>
            <h3>使用与运行分开</h3>
            <p style="border:0">计算与存储统一在企业数据中心内进行，通过专有网络将结果返回给本地终端，本地终端只负责显示</p>
        </div>
    </div>
</div>
<div class="deskbox02 ov" style="padding:7px 0;">
    <div style="background:#f5f6f8; ">
        <div class="w1000 ov">        		
            <div class="msg z" style="margin:40px 0 40px 0;">
                <h2>阳光云桌面的技术</h2>
                <p>通过应用云计算技术，将服务器和存储集群化，利用虚拟化技术实现基础设施、桌面、应用等资源的共享，并对其进行集中部署和管理，在数据中心统一托管以服务方式交付桌面的云平台系统。</p>
                <h3>阳光云桌面的实现</h3>
                <p><span>用户端只有瘦终端+终端套件（显示器+鼠标+键盘）。云服务器在有孚机房。通过光纤连接。</span><br />
                    <span>用户通过云终端设备或安装了云钥的PC等设备访问阳光云平台系统，即可获得与通常个人PC使用感觉近似无差异化的用户体验</span></p>
                <h3>阳光云桌面的目标</h3>
                <p style="border:0;">上海有孚计算机网络有限公司成立于2001年,是中国顶级互联网网络服务解决方案供应商和云计算数据中心运营商。有孚还按国际标准打造高规格的云计算数据中心并结合强大的...</p>
            </div>
            <?php echo CHtml::image(Yii::app()->BaseUrl . "/images/img-desk2.gif", "desk", array('height' => 370, 'width' => 450, 'class' => 'y')); ?>
        </div>
    </div>
</div>

<div class="deskbox03 deskn ov" >
    <div class="w1000 ov">   
        <?php echo CHtml::image(Yii::app()->BaseUrl . "/images/img-desk3.gif", "desk", array('height' => 416, 'width' => 430, 'class' => 'z')); ?>          		
        <div class="msg y">
            <h2 class="deskh2">使用便利</h2>
            <h3>单用户多桌面</h3>
            <p><span>为一个用户同时分配多个桌面，非常适合于跨项目运作的员工，尤其是在软件外包等特殊行业</span><br />
                <span>实现企业在项目管理和人员管理方面的融合，在技术上可以更好的促进矩阵式的先进管理模式。</span></p>
            <h3>用户自管理</h3>
            <p>用户能够通过热键进入自管理界面，对虚拟桌面进行重启等相关活动，减轻数据中心管理员工作压力.</p>
            <h3>移动办公</h3>
            <p style="border:0;">方便的进行移动办公，出差在外员工可方便获取自己的办公桌面及办公数据</p>
        </div>
    </div>
</div>

<div class="deskbox04 deskn ov" style="padding:7px 0;">
    <div style="background:#f5f6f8;padding:30px 0; ">
        <div class="w1000 ov">        		
            <div class="msg z" style="margin:40px 0 40px 0; width:530px;">
                <h2>方便管理</h2>
                <h3>批量创建阳光云桌面</h3>
                <p>可根据既定模版批量创建多个云桌面系统，每个云桌面只需10秒钟</p>
                <h3>灵活修改阳光云桌面配置</h3>
                <p>用户能够通过热键进入自管理界面，对虚拟桌面进行重启等相关活动，减轻数据中心管理员工作压力。</p>
                <h3>集中管理云终端</h3>
                <p style="border:0;">管理员可在数据中心集中管理用户端设备</p>
            </div>
            <?php echo CHtml::image(Yii::app()->BaseUrl . "/images/img-desk4.gif", "desk", array('height' => 399, 'width' => 442, 'class' => 'y')); ?>
        </div>
    </div>
</div>

<div class="deskbox04 deskn ov">
    <div class="w1000 ov" style="padding-top:20px;">   
        <?php echo CHtml::image(Yii::app()->BaseUrl . "/images/img-desk5.gif", "desk", array('height' => 412, 'width' => 427, 'class' => 'z')); ?>            		
        <div class="msg y" style="margin:40px 0 70px 0; width:530px;">
            <h2>高可靠性</h2>
            <h3>HA高可用构架</h3>
            <p><span>用户虚拟桌面的镜像存储于高可用性存储中，集群中的服务器本身仅供执行虚拟机，不存储虚拟机的长期状态。</span><br />
                <span>单点故障的排除</span></p>
            <h3>云桌面动态迁移</h3>
            <p>在虚拟机在线的情况下，按照手工将虚拟桌面迁移至其他服务器设备上，在不影响用户使用的情况下，满足设备调试与维护方面需要（零宕机）</p>
            <h3>容错机制</h3>
            <p style="border:0;"><span>对于用户而言，永远有另一个同步的虚拟机做备份，一旦因为服务器意外宕机，另一台服务器的备份虚拟机会马上接替工作。</span><br /><span>最大程度保障用户工作连续性</span></p>
        </div>
    </div>
</div>

<div class="deskbox04 deskn ov" style="padding:7px 0;">
    <div style="background:#f5f6f8;padding:30px 0; ">
        <div class="w1000 ov">        		
            <div class="msg z" style="margin:40px 0 40px 0; width:530px;">
                <h2>高安全性</h2>
                <h3>VDI构架与桌面传输安全</h3>
                <p><span>本地无数据处理与存储</span><Br />
                    <span>矢量传输与SSL加密</span></p>
                <h3>存储加密</h3>
                <p>存储加密安全管理与虚拟桌面构架整合</p>
                <h3>桌面备份与容灾</h3>
                <p style="border:0;">采用虚拟桌面构架可以对桌面实现多层次的安全备份，管理人员能够利用赛科DMS管理工具，建立备份策略，批量备份阳光云桌面</p>
            </div>
            <?php echo CHtml::image(Yii::app()->BaseUrl . "/images/img-desk6.gif", "desk", array('height' => 390, 'width' => 445, 'class' => 'y')); ?>
        </div>
    </div>
</div>

<div class="deskbox04 deskn ov">
    <div class="w1000 ov" style="padding:40px;">   
        <?php echo CHtml::image(Yii::app()->BaseUrl . "/images/img-desk7.gif", "desk", array('height' => 347, 'width' => 445, 'class' => 'z')); ?>        		
        <div class="msg y" style="margin:40px 0 70px 0; width:530px;">
            <h2>共享与节能</h2>
            <h3>资源复用</h3>
            <p>云计算的核心观念是资源共享与复用，阳光云桌面解决方案充分发挥了这一特色；在保障用户优秀使用体验的前提下，实现资源充分利用、高度共享与复用</p>
            <h3>节能减排</h3>
            <p style="border:0;">阳光云桌面较传统PC能源消耗方面能够节能70%以上，实实在在的帮您省钱</p>    				
        </div>
    </div>
</div>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'mydialog',
    'options' => array(
        'title' => '',
        'modal' => true,
        'autoOpen' => false,
        'height' => '645',
        'width' => '1000',
        'resizable' => false,
    ),
));
?>
<div class="win02 w990 ov">
    <div class="win02_toph"><h1>免费试用</h1><a href="javascript:void(0);" class="close y"> </a></div>
    <div class="trial" style="text-align:center;display:none;">
        <?php echo CHtml::image(Yii::app()->BaseUrl . "/images/img-trail-suc.gif", "trial-suc", array('height' => 89, 'width' => 228, 'style' => 'margin-top:140px;')); ?>  
        <p style="font-size:16px;margin: auto;">我们会尽快联系您 !  <span style="font-size:24px;color:#31bcb7;margin:0 10px;" id="counter-clock">3</span> 秒后关闭···</p>
    </div>
    <div class="trial">
        <?php echo CHtml::image(Yii::app()->BaseUrl . "/images/winbg-trial.gif", "trial", array('class' => 'z', 'style' => 'margin:50px 0 0 60px;')); ?>  
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'Contact-form',
            'enableAjaxValidation' => true,
            'action' => array('index'),
            'htmlOptions' => array(
                'class' => 'y',
            ))
        );
        ?>
        <p class="info">请留下您的联系方式，我们会尽快安排工作人员与您联系</p>

        <dl>
            <dt>单位名称<font> * </font>：</dt>
            <dd><?php echo $form->textField($model, 'company', array('class' => 'int')); ?>
                <span class="txt" style="color:#F00"><?php echo $form->error($model, 'company'); ?></span>
            </dd>
        </dl>
        <dl>
            <dt>个人职务<font> * </font>：</dt>
            <dd><?php echo $form->textField($model, 'individual_duties', array('class' => 'int')); ?>
                <span class="txt" style="color:#F00"><?php echo $form->error($model, 'individual_duties'); ?></span>
            </dd>
        </dl>
        <dl>
            <dt>联系人姓名<font> * </font>：</dt>
            <dd><?php echo $form->textField($model, 'username', array('class' => 'int')); ?>
                <span class="txt" style="color:#F00"><?php echo $form->error($model, 'username'); ?></span>
            </dd>
        </dl>
        <dl>
            <dt>联系电话<font> * </font>：</dt>
            <dd><?php echo $form->textField($model, 'mobile', array('class' => 'int')); ?>
                <span class="txt" style="color:#F00"><?php echo $form->error($model, 'mobile'); ?></span>
            </dd>
        </dl>
        <dl>
            <dt>地址<font> * </font>：</dt>
            <dd><?php echo $form->textField($model, 'addr_more', array('class' => 'int', 'style' => 'width:310px;')); ?>
                <?php echo $form->error($model, 'addr_more', array('style' => 'margin-top:3px;')); ?>
            </dd>
        </dl>
        <dl>
            <dt>需求数量<font> * </font>：</dt>
            <dd><?php echo $form->textField($model, 'demandnum', array('class' => 'int')); ?>
                <span class="txt">台</span>
                <span class="txt" style="color:#F00"> <?php echo $form->error($model, 'demandnum'); ?></span>
            </dd>
        </dl>
        <dl>
            <dt>备注：</dt>
            <dd><?php echo $form->textArea($model, 'remark'); ?></dd>
        </dl>
        <div class="ov" style="margin:20px auto; text-align:center">
            <?php
            echo CHtml::ajaxSubmitButton('', CHtml::normalizeUrl(array('desktop/index')), array(
                'success' => 'function(data){
                        if(data.status=="1")
                        {
                        $("form").find("input,textarea").val("");
                        $(".errorMessage").hide();
                        $("#yt0").attr("disabled",false);$("div.trial:first").show().next().hide();
                            var counter=setInterval(function(){
                            var txt=$("#counter-clock").text();
                            $("#counter-clock").text(parseInt(txt)-1);
                            if((parseInt(txt)-1)<1){
                                 clearInterval(counter);
                                    $("#mydialog").dialog("close");
                                     $("div.trial:eq(1)").show().prev().hide();
                                }
                            },1000);
                            }else{
                            $("form :submit").attr("disabled",false);
                         }
                        }',
                'dataType' => 'json'), array("class" => "sub", 'onclick' => '$(this).attr("disabled",true);', 'style' => 'cursor:pointer;')
            );
            ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
<?php
$this->endWidget();
?>
<?php Yii::app()->getClientScript()->registerCssFile($this->baseUrl . "/css/win.css"); ?>
<?php Yii::app()->getClientScript()->registerScript("close-dialog", '$("a.close").click(function(){$("#mydialog").dialog("close");$("div.trial:eq(1)").show().prev().hide();});', CClientScript::POS_READY); ?>
