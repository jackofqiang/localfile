<?php
/* @var $this AppController */
/* @var $model App */

$this->breadcrumbs=array(
	'Apps'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'List App', 'url'=>array('admin')),
	array('label'=>'Manage App', 'url'=>array('admin')),
);
?>

<h1>Create App</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>