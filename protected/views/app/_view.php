<?php
/* @var $this AppController */
/* @var $data App */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('appname')); ?>:</b>
	<?php echo CHtml::encode($data->appname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('summary')); ?>:</b>
	<?php echo CHtml::encode($data->summary); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('detail')); ?>:</b>
	<?php echo CHtml::encode($data->detail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('applogo')); ?>:</b>
	<?php echo CHtml::encode($data->applogo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('appcategory')); ?>:</b>
	<?php echo CHtml::encode($data->appcategory); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('buyurl')); ?>:</b>
	<?php echo CHtml::encode($data->buyurl); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('author')); ?>:</b>
	<?php echo CHtml::encode($data->author); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_time')); ?>:</b>
	<?php echo CHtml::encode($data->update_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_time')); ?>:</b>
	<?php echo CHtml::encode($data->create_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isrecommend')); ?>:</b>
	<?php echo CHtml::encode($data->isrecommend); ?>
	<br />

	*/ ?>

</div>