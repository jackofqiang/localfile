<?php
/* @var $this AppController */
/* @var $model App */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'app-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'appname'); ?>
		<?php echo $form->textField($model,'appname',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'appname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'summary'); ?>
		<?php echo $form->textField($model,'summary',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'summary'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'detail'); ?>
		<?php echo $form->textArea($model,'detail',array('rows'=>10, 'cols'=>80)); ?>
		<?php echo $form->error($model,'detail'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'applogo'); ?>
		<?php echo $form->textField($model,'applogo',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'applogo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'appcategory'); ?>
                <select name="App[appcategory]">
                    <option value="1">CRM</option>
                    <option value="2">协同办公</option>
                    <option value="3">电商</option>
                    <option value="4">论坛</option>
                    <option value="5">博客</option>
                </select>
		
		<?php echo $form->error($model,'appcategory'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'buyurl'); ?>
		<?php echo $form->textField($model,'buyurl',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'buyurl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'author'); ?>
		<?php echo $form->textField($model,'author',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'author'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'update_time'); ?>
		<?php echo $form->textField($model,'update_time'); ?>
		<?php echo $form->error($model,'update_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'create_time'); ?>
		<?php echo $form->textField($model,'create_time'); ?>
		<?php echo $form->error($model,'create_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'isrecommend'); ?>
		<?php echo $form->textField($model,'isrecommend'); ?>
		<?php echo $form->error($model,'isrecommend'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->