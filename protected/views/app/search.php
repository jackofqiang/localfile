<?php
/* @var $this PlatController */

$this->pageTitle = Yii::app()->name . "--云平台";
?>


<div class="banner ov">
    <div class="w1000"><img src="images/banner-paas.gif" /></div>
</div>

<div class="w1000 ov">
    <div class="sidebar ov z">
        <div class="sidebar-nav mb13 ov">
            <form class="appsearch ov" action="?r=app/search" method="post">
                <input type="text" class="txt z" name="appname" value="<?php echo $appname; ?>" onfocus="if (this.value == '寻找您需要的APP') {
                            this.value = '';
                        }" onblur="if (this.value == '') {
                            this.value = '寻找您需要的APP';
                        }" />
                <input type="submit" value=" " class="sub z" />
            </form>
            <ul>
                <li class="on"><a href="javascript:;">CRM</a></li>
                <li><a href="javascript:;">协同办公</a></li>
                <li><a href="javascript:;">电商</a></li>
                <li><a href="javascript:;">论坛</a></li>
                <li><a href="javascript:;">博客</a></li>
            </ul>
        </div>

        <a href="javascript:;"><img src="images/pro-ad1.gif" class=" mb13" /></a>
        <a href="javascript:;"><img src="images/pro-ad2.gif" class=" mb13"  /></a>
        <a href="javascript:;"><img src="images/pro-ad3.gif" class=" mb13" /></a>
        <a href="javascript:;"><img src="images/pro-ad4.gif" class=" mb13"  /></a>
    </div>

    <div class="paas-right ov y">
        <ul class="applist">

            <?php if (!empty($apps)) {
                foreach ($apps as $app) {
                    ?>
                    <li><a href="javascript:;"><img src="images/appimg1.gif" class="z" /></a>
                        <div class="y"><a href="javascript:;" class="tit"><?php echo $app->appname; ?></a><p><?php echo $app->summary; ?></p><a href="javascript:;" class="buy">点击购买</a></div>
                    </li>
                <?php }
            } else {
                ?>
                没有相关数据的APP！！
<?php } ?>
        </ul>

        <div class="page ov y">
            <div id="pager">    
                <?php
                $this->widget('CLinkPager', array(
                    'header' => '',
                    'footer' => '',
                    'pages' => $pages,
                    'maxButtonCount' => 13,
                        )
                );
                ?>    
            </div>
        </div>
    </div>
</div>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'mydialog',
    // additional javascript options for the dialog plugin
    'options' => array(
        'title' => '',
        'autoOpen' => false,
        'width' => '635',
        'height' => '355',
        'modal' => true
    ),
));
?>
<div class="ad"><img src="images/ad-0901.jpg" /></div>
    <?php
    $this->endWidget('zii.widgets.jui.CJuiDialog');
    ?>
<script type="text/javascript" >
                    $(function() {
                        $("ul li").click(function() {
                            $(this).addClass("on").siblings().removeClass("on");
                            var i = $(this).index();
                            var url = "?r=plat/index";
                            $(".paas-right").load(url, {appcategory: i + 1}, function(retValue) {
                                if (retValue) {
                                }

                            });

                        });
                        $("ul.topmenu li").eq(1).addClass("on").siblings().removeClass("on");
                        $(document).delegate(".buy", "click", function() {
                            $("#mydialog").dialog("open");
                        });
                    });
</script>
