<?php
/* @var $this PlatController */

$this->pageTitle = Yii::app()->name . "--云平台";
//Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/jquery-ui-1.10.3.custom.min.js"); 
//Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl."/css/jquery-ui-1.10.3.custom.min.css");
?>


<div class="banner ov">
    <div class="w1000"><?php echo  CHtml::image(Yii::app()->BaseUrl."/images/banner-paas.gif","picture");?></div>
</div>

<div class="w1000 ov">
    <div class="sidebar ov z">
        <div class="sidebar-nav mb13 ov">
            <form class="appsearch ov" action="?r=app/search" method="post">
                <input type="text" class="txt z" name="appname" value="寻找您需要的APP" onfocus="if (this.value == '寻找您需要的APP') {
                            this.value = '';
                        }" onblur="if (this.value == '') {
                            this.value = '寻找您需要的APP';
                        }" />
                <input type="submit" value=" " class="sub z" />
            </form>
            <ul>
                <li class="on"><a href="javascript:;">CRM</a></li>
                <li><a href="javascript:;">协同办公</a></li>
                <li><a href="javascript:;">电商</a></li>
                <li><a href="javascript:;">论坛</a></li>
                <li><a href="javascript:;">博客</a></li>
            </ul>
        </div>

        <a href="javascript:;"><?php echo  CHtml::image(Yii::app()->BaseUrl."/images/pro-ad1.gif","picture",array('class'=>'mb13'));?></a>
        <a href="javascript:;"><?php echo  CHtml::image(Yii::app()->BaseUrl."/images/pro-ad2.gif","picture",array('class'=>'mb13'));?></a>
        <a href="javascript:;"><?php echo  CHtml::image(Yii::app()->BaseUrl."/images/pro-ad3.gif","picture",array('class'=>'mb13'));?></a>
        <a href="javascript:;"><?php echo  CHtml::image(Yii::app()->BaseUrl."/images/pro-ad4.gif","picture",array('class'=>'mb13'));?></a>
    </div>

    <div class="paas-right ov y">
        <ul class="applist">
            <?php foreach ($apps as $k => $app) { ?>
                <li><a href="javascript:;"><?php echo  CHtml::image(Yii::app()->BaseUrl."/images/appimg1.gif","buy",array('class'=>'z'));?></a>
                    <div class="y"><a href="javascript:;" class="tit"><?php echo $app->appname; ?></a><p><?php echo $app->summary; ?></p><a href="javascript:;" class="buy">点击购买</a></div>
                </li>
            <?php } ?>
            <?php if ($k % 2 == 0) { ?>
                <li></li>
            <?php } ?>
        </ul>
        <div class="cl ov"></div>
        <div class="page ov y"> 
            <div id="pager">    
                <?php
                $this->widget('CLinkPager', array(
                    'header' => '',
                    'footer' => '',
                    'prevPageLabel' => "",
                    'nextPageLabel' => "",
                    'pages' => $pages,
                    'maxButtonCount' => 13,
                        )
                );
                ?>    
            </div>
        </div>
    </div>
</div>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'mydialog',
    // additional javascript options for the dialog plugin
    'options' => array(
        'title' => '',
        'autoOpen' => false,
        'width' => '635',
        'height' => '355',
        'modal' => true
    ),
));
?>
<div class="ad"><?php echo  CHtml::image(Yii::app()->BaseUrl."/images/ad-0901.jpg","picture");?></div>
    <?php
    $this->endWidget('zii.widgets.jui.CJuiDialog');
    ?>
<script type="text/javascript" >
                    $(function() {
                        $("ul li").click(function() {
                            $(this).addClass("on").siblings().removeClass("on");
                            var i = $(this).index();
                            var url = "?r=plat/index";
                            $(".paas-right").load(url, {appcategory: i + 1}, function(retValue) {
                                if (retValue) {
                                }
                            });
                        });
                        $("ul.topmenu li").eq(1).addClass("on").siblings().removeClass("on");
                        $(document).delegate(".buy", "click", function() {
                            $("#mydialog").dialog("open");
                        });
                    });

</script>