<?php
/* @var $this SolutionController */

$this->pageTitle = Yii::app()->name . "--云方案";
?>

<div class="banner ov">
    <div class="w1000"><?php echo CHtml::image(Yii::app()->BaseUrl . "/images/banner-project.jpg", "picture"); ?></div>
</div>

<div class="w1000 ov">
    <div class="sidebar mb13 ov z">
        <div class="sidebar-nav mb13 ov">
            <h1>云方案</h1>
            <ul class="left-menu">
                <li class="on"><a href="javascript:;">电子商务</a></li>
                <li><a href="javascript:;">游戏视频</a></li>
                <li><a href="javascript:;">金融证券</a></li>
                <li><a href="javascript:;">远程教育</a></li>
                <li><a href="javascript:;">中小企业</a></li>
                <li><a href="javascript:;">政府机构</a></li>
            </ul>
        </div>
        <div class="side-connect">
            
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'Message-form',
                'enableAjaxValidation' => true,
            ));
            ?>
            
            <dl>
                <dt>单位名称：</dt><dd><?php echo $form->textField($model, 'company', array('class' => 'txt mb13')); ?><?php echo $form->error($model, 'company'); ?></dd>
                <dt>所属行业：</dt><dd><?php echo $form->textField($model, 'industry', array('class' => 'txt mb13')); ?>
                    <?php echo $form->error($model, 'industry'); ?></dd>
                <dt>联系人姓名：</dt><dd><?php echo $form->textField($model, 'contact', array('class' => 'txt mb13')); ?>
                    <?php echo $form->error($model, 'contact'); ?></dd>
                <dt>联系电话：</dt><dd><?php echo $form->textField($model, 'mobile', array('class' => 'txt mb13')); ?>
                    <?php echo $form->error($model, 'mobile'); ?></dd>
                <dt>需求描述：</dt><dd><?php echo $form->textArea($model, 'demand'); ?></dd>
            </dl>

            <?php echo CHtml::ajaxSubmitButton('', CHtml::normalizeUrl(array('solution/')), array('success' => 'function(data){alert(data.message);if(data.status=="1"){$("form").find("input,textarea").val("");$(".errorMessage").hide();$("#yt0").attr("disabled",false);}location.reload();}', 'dataType' => 'json'), array("class" => "sub y",'onclick'=>'$(this).attr("disabled",true);')); ?>

            <?php $this->endWidget(); ?>

        </div>
    </div>

    <div class="project-right ov y" >
        <h2>电子商务解决方案</h2>
        <p>随着电子商务的不断发展，众多企业逐渐意识到电子商务的重要性，不仅积极尝试建设自己企业的"网络门面"，并开始寻求除域名、主机、邮箱等基础IDC业务外的服务。</p>

        <p>有孚的电子商务解决方案，根据各企业的不同需求，提供从网站建设、网络运行环境搭建、后续增值服务等一站式服务，减轻客户运营压力。利用有孚云计算数据中心提供的高品质网络带宽确保电商企业在峰值时段大量用户的并发访问。提供冗余的架构设计，实现7*24*365天不间断服务，确保网站使用的可靠性，提高客户企业的整体利益。</p>
        <center><?php echo CHtml::image(Yii::app()->BaseUrl . "/images/project-right-img.gif", "picture"); ?></center>

    </div>
    <div class="project-right ov y" style="display:none;">
        <h2>游戏视频解决方案</h2>
        <p>网络游戏在近年来发展迅速，除了传统网络端游戏之外，网页游戏、手机游戏等快速发展，也让整个游戏市场愈发壮大。</p>
        <p>有孚通过云计算数据中心提供充足的带宽出口可解决游戏用户集中在线、客户端下载、页面访问等峰值负载，双线路的接入可解决游戏的延迟让用户有更流畅的游戏体验。借助有孚的负载均衡服务可以集合多台主机的服务能力，随时实现水平扩展、系统扩容，增加对游戏服务数据的负载量。配合应用架构设计，有效避免单点故障，最大限度的保持游戏的在线状态。</p>

        <center><?php echo CHtml::image(Yii::app()->BaseUrl . "/images/project-solution_yx.gif", "picture"); ?></center>

    </div>
    <div class="project-right ov y" style="display:none;">
        <h2>金融证券解决方案</h2>
        <p>金融证券类网站，已发展为包含"实时行情、网上营业厅、网上客服、在线交易"等多种个性化互动功能模块为一体的综合性服务平台，也是用户获得信息、做出决策的首要渠道。</p>
        <p>有孚的金融证劵解决方案利用云计算数据中心提供的高品质带宽双线路接入，保障用户的体验感受、实时证券信息数据的捕获。结合有孚的专线加密技术、网络隔离技术有效地防御病毒和黑客入侵，确保用户隐私安全、在线支付安全，同时采用监控服务保证网站平衡稳定的性能，提升公司的品牌形象及业务开展。</p>
        <center><?php echo CHtml::image(Yii::app()->BaseUrl . "/images/project-solution_jr.gif", "picture"); ?></center>

    </div>
    <div class="project-right ov y" style="display:none;">
        <h2>远程教育解决方案</h2>
        <p>远程教育是教学领域新的新型管理方式，引领时代的潮流，同时也体现了一个学校、机构利用高科技实现快捷、高效、低成本，高效率等多方面的优势。</p>
        <p>有孚的远程教育解决方案，依托有孚云计算数据中心提供灵活的带宽控制，能解决高峰期的大量用户访问，空闲时降低带宽速率，减少运营成本。有孚高品质的网络带宽和双线路的接入解决全国各地用户访问和课件的下载，降低实时课程演示的传输延迟，提高演示的清晰度和流畅度。</p>

        <center><?php echo CHtml::image(Yii::app()->BaseUrl . "/images/project-solution_yc.gif", "picture"); ?></center>

    </div>
    <div class="project-right ov y" style="display:none;">
        <h2>中小企业解决方案</h2>
        <p>随着中国经济快速发展，中小企业不断涌现，企业信息化办公需求日益扩展。</p>
        <p>有孚的中小企业解决方案，根据企业的不同需求，利用云计算数据中心提供硬件平台和云计算平台，大幅降低自建机房的高成本。通过经验丰富的售前技术人员搭建网络架构以及专业的售后技术人员进行运维维护，省去客户的人员投入，减轻了客户的运营压力，降低客户的运营成本。</p>

        <center><?php echo CHtml::image(Yii::app()->BaseUrl . "/images/project-solution_qy.gif", "picture"); ?></center>

    </div>
    <div class="project-right ov y" style="display:none;">
        <h2>政府机构解决方案</h2>
        <p>政府机构网站是政府部门展示形象的重要窗口，是提供信息发布的平台、与市民互动的平台。<p>
        <p>有孚政府机构解决方案，依托云数据计算中心，结合有孚安全顾问服务，将源站保护起来。既能保障其内容不会被恶意篡改，又能将各类攻击影响降到最低。确保网站信息的正确性、政府服务的可用性和信息反馈的畅通性。依托有孚提供的数据备份方案，确保核心信息数据的安全性、可靠性，保障政府网站运行的稳定性。</p>

        <center><?php echo CHtml::image(Yii::app()->BaseUrl . "/images/project-solution_zf.gif", "picture"); ?></center>

    </div>
</div>

<script type="text/javascript" >
    $(function() {
        $("ul.left-menu li").click(function() {
            $(this).addClass("on").siblings().removeClass("on");
            var index = $(this).index();
            $(".project-right").eq(index).show().siblings(".project-right").hide();
        });
    });

</script>
