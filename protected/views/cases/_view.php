<?php
/* @var $this CasesController */
/* @var $data Cases */
?>

<div class="case" style=" border-top:1px solid #e0e0e0; margin-top:5px;">
    <div class="w1000 ov">
        <img class="img" src="<?php echo CHtml::encode($data->imgpath); ?>" />
        <div class="msg y">
            <h3><?php echo CHtml::encode($data->title); ?></h3>
            <p class="p1"><?php echo CHtml::encode($data->summary); ?>...</p>
            <dl><dt>使用产品</dt><dd class="dd1">云主机</dd><dd class="dd2">云桌面</dd></dl>
            <strong><?php echo CHtml::encode($data->getAttributeLabel('userexperience')); ?></strong>
            <p class="p2"><?php echo CHtml::encode($data->userexperience); ?>...</p>
        </div>
    </div>
</div>
