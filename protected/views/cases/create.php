<?php
/* @var $this CasesController */
/* @var $model Cases */

$this->breadcrumbs = array(
    'Cases' => array('admin'),
    'Create',
);

$this->menu = array(
    array('label' => 'List Cases', 'url' => array('admin')),
    array('label' => 'Manage Cases', 'url' => array('admin')),
);
?>

<div  style="margin:0 auto;">
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>

</div>