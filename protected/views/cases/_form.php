
<div class="form" style="margin-left:400px;margin-top:100px;margin-bottom:100px;">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'cases-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>


    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title', array('size' => 32, 'maxlength' => 32)); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'summary'); ?>
        <?php echo $form->textField($model, 'summary', array('size' => 60, 'maxlength' => 64)); ?>
        <?php echo $form->error($model, 'summary'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'detail'); ?>
        <?php echo $form->textArea($model, 'detail', array('rows' => 6, 'cols' => 50,'maxLength'=>128)); ?>
        <?php echo $form->error($model, 'detail'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'imgpath'); ?>
        <?php  if(isset($model->imgpath)) echo CHtml::image(Yii::app()->BaseUrl . "/".$model->imgpath, "image",array('height'=>'296px','width'=>'450px'))."<br/>"; ?>
         <?php echo $form->FileField($model, 'imgpath', array('size' => 60, 'maxlength' => 128)); ?>
        <?php echo $form->error($model, 'imgpath'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'product'); ?>
        <?php echo $form->checkBoxList($model, 'product', array('云桌面' => '云桌面', '云主机' => '云主机'), array('separator' => '&nbsp;&nbsp;', 'labelOptions' => array('class' => 'checkboxlabel','style'=>'display:inline-block;margin:0px 5px;'))); ?>
        <?php echo $form->error($model, 'product'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'userexperience'); ?>
        <?php echo $form->textArea($model, 'userexperience', array('rows' => 6, 'cols' => 50,'maxLength'=>512)); ?>
        <?php echo $form->error($model, 'userexperience'); ?>
    </div>



    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
    $(function(){
             $('textarea[name]').each(function(i){
                  $(this).change(function(){
                      var $this=$(this);
                      if(i==0&&$this.val().length > 128){
                          $this.val($this.val().substr(0,128));                         
                      }
                      if(i==1&&$this.val().length > 512){
                         $this.val($this.val().substr(0,512));    
                      }   
                  });
    });
    });
</script>