<?php
$this->pageTitle = Yii::app()->name . "--客户案例";
?>
<div class="banner ov">
    <div class="w1000"><?php echo CHtml::image(Yii::app()->BaseUrl . "/images/banner-case.gif", "picture"); ?></div>
</div>
<div class='main'>
    <?php foreach ($articles as $article) { ?>
        <div class="case" style=" border-top:1px solid #e0e0e0; margin-top:5px;">
            <div class="w1000 ov">
                <img class="img" src="<?php echo Yii::app()->BaseUrl . '/' . $article->imgpath; ?>" />
                <div class="msg y">
                    <h3><?php echo $article->title; ?></h3>
                    <p class="p1"><?php echo $article->detail; ?></p>
                    <dl stylel="overflow:hidden;"><dt>使用产品</dt><?php
                        $rows = explode(",", $article->product);
                        foreach ($rows as $row) {
                            if (!empty($row)) {
                                ?><dd class="dd1"><?php echo $row; ?></dd><?php
                            }
                        }
                        ?></dl>
                    <strong>用户感受</strong>
                    <p class="p2"><?php echo $article->userexperience; ?></p>
                </div>
            </div>
        </div> 
    <?php } ?>
    <div class="pagebar ov" style="overflow:hidden;margin-top:5px;margin-bottom:5px;display:block;">
        <div id="pager" class="y">    
            <?php
            $this->widget('CLinkPager', array(
                'header' => '',
                'footer' => '',
                'prevPageLabel' => "",
                'nextPageLabel' => "",
                'pages' => $pages,
                'maxButtonCount' => 10,
                "selectedPageCssClass" => 'on',
                'cssFile' => Yii::app()->baseUrl . '/css/page.css',
                'htmlOptions' => array(),
                    )
            );
            ?>    
        </div>
    </div>
</div>
