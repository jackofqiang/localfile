<?php
/* @var $this EnumController */
/* @var $model Enum */

$this->breadcrumbs=array(
	'Enums'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Enum', 'url'=>array('admin')),
	array('label'=>'Manage Enum', 'url'=>array('admin')),
);
?>

<h1>Create Enum</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>