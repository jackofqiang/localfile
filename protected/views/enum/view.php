<?php
/* @var $this EnumController */
/* @var $model Enum */

$this->breadcrumbs=array(
	'Enums'=>array('admin'),
	$model->description,
);

$this->menu=array(
	array('label'=>'List Enum', 'url'=>array('admin')),
	array('label'=>'Create Enum', 'url'=>array('create')),
	array('label'=>'Update Enum', 'url'=>array('update', 'id'=>$model->id)),
//	array('label'=>'Delete Enum', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Enum', 'url'=>array('admin')),
);
?>

<h1>View Enum #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'category',
		'description',
	),
)); ?>
