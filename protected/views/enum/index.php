<?php
/* @var $this EnumController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Enums',
);

$this->menu=array(
	array('label'=>'Create Enum', 'url'=>array('create')),
	array('label'=>'Manage Enum', 'url'=>array('admin')),
);
?>

<h1>Enums</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
