<?php
/* @var $this EnumController */
/* @var $model Enum */

$this->breadcrumbs=array(
	'Enums'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Enum', 'url'=>array('admin')),
	array('label'=>'Create Enum', 'url'=>array('create')),
	array('label'=>'View Enum', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Enum', 'url'=>array('admin')),
);
?>

<h1>Update Enum <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>