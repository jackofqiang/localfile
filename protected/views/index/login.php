<?php

$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login',
);
?>
<style type="text/css">
    .row{
        margin:10px;
    }
    input{}
    p{margin:10px;font-size:10px;}
</style>
<div style="margin:30px auto;margin-left:400px;width:400px; height:300px;border:1px solid red;">
    <div class="form"  style="margin:30px auto;margin-left:100px">
        <h1>登录</h1>

        <p>请输入你的用户名和密码：</p>

        <div class="form" >
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'login-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>

            <p class="note">请登录</p>

            <div class="row">
                <?php echo $form->labelEx($model, 'username'); ?>
                <?php echo $form->textField($model, 'username'); ?>
                <?php echo $form->error($model, 'username'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model, 'password'); ?>
                <?php echo $form->passwordField($model, 'password',array('autocomplete'=>'off')); ?>
                <?php echo $form->error($model, 'password'); ?>
            </div>

            <div class="row rememberMe">
                <?php echo $form->checkBox($model, 'rememberMe'); ?>
                <?php echo $form->label($model, 'rememberMe'); ?>
                <?php echo $form->error($model, 'rememberMe'); ?>
            </div>

            <div class="row buttons">
                <?php echo CHtml::submitButton('登录'); ?>
            </div>

            <?php $this->endWidget(); ?>
        </div><!-- form -->

    </div>

</div>
