<?php
$this->pageTitle = Yii::app()->name;
?>
<div class="index-banner ov">
    <div class="w1000">
        <div id="container">
            <div id="slides">
                <div class="slides_container">
                    <div class="slide">
                        <a href="javascript:;" style='cursor:text;'> <?php echo CHtml::image($this->baseUrl . "/images/hd-index.jpg", "dynamic-ad"); ?></a>
                    </div>
                    <!--                    <div class="slide ">
                                            <a href="<?php echo $this->createUrl('solution/'); ?>"> <?php echo CHtml::image($this->baseUrl . "/images/hd-02.jpg", "dynamic-ad"); ?></a>
                                        </div>
                                        <div class="slide ">
                                            <a href="<?php echo $this->createUrl('desktop/'); ?>"> <?php echo CHtml::image($this->baseUrl . "/images/hd-03.jpg", "dynamic-ad"); ?></a>
                                        </div>-->
                </div>
            </div>
        </div> 
    </div>
</div>
<div class="mn w1000"></div>
<div class="mn-pro2 ov">
    <div class="w1000">
        <div id="tb2_" class="tb2_">
            <ul>
                <li id="tb2_1"  class="hovertab2 "  onmouseover="x:HoverLi2(1);">
                    <div class="box box01 z">
                        <p class="txt">有孚打造的阳光云平台是一个拥有软件市场和虚拟资源池的平台，为软件厂商和企业用户提供一站式服务，软件厂商可以及时发布软件，企业用户可以通过阳光云平台及时购买部署，方便客户同时降低双方的投入成本，助力传统软件企业转型SAAS运营，实现多元化发展 。</p>
                        <a href="<?php echo $this->createUrl('sunapp/'); ?>" class="bt">产品详情</a>
                    </div>
                </li>
                <li id="tb2_2" class="normaltab2" onmouseover="i:HoverLi2(2);">
                    <div class="box box02 z">
                        <p class="txt">阳光云方案是基于云计算的现代化数据中心的解决方案。根据客户实际需求，对IT资源进行整合，集成公共资源池，通过网络按需快速调配和释放。以购买服务的方式实现低成本使用计算资源，根据业务变化情况下进行快速扩展，提升设备利用率，大幅减少IT管理，从而提供更加有效率的应用服务。</p>
                        <a href="<?php echo $this->createUrl('solution/'); ?>" class="bt">产品详情</a>
                    </div>
                </li>
                <li id="tb2_3" class="normaltab2" onmouseover="i:HoverLi2(3);" style="margin-right:0">
                    <div class="box box03 y">
                        <p class="txt">您的PC是否存在安全性差、管理困难、便利性差、灵活性低的问题？还是发现支出成本越来越高？阳光云桌面可以帮您解答。这是一个基于云计算技术提供的虚拟办公桌面，将传统的使用与运行分开，达到一个安全、有效、易管理的桌面效果，同时可以减少您隐性成本的支出。</p>
                        <a href="<?php echo $this->createUrl('desktop/'); ?>" class="bt">产品详情</a>
                    </div>
                </li>              
            </ul>
        </div>

        <div class="ctt0" style="height:0;overflow:hidden;clear:both;">
            <div class="dis" id="tbc2_01"></div>
            <div class="undis" id="tbc2_02"></div>
            <div class="undis" id="tbc2_03"></div>
        </div>
    </div>
</div>
<div class="dynamic ov">
    <div class="w1000">
        <div class="dynamic-top">
            <h3 class='z'>
                <a href="<?php echo $this->createUrl('article/index'); ?>">阳光动态</a>
            </h3>
            <div class="more y" style="cursor:pointer;" onclick="location.href = '<?php echo $this->createUrl('article/index'); ?>';">
                <a href="<?php echo $this->createUrl('article/index'); ?>">更多 >></a>
            </div>
        </div>
        <div class="z" style="width:565px;">    
            <div id="tb1_" class="tb1_">
                <ul>
                    <li id="tb1_1"  style="border-left:none;" class="hovertab1" onmouseover="x:HoverLi1(1);"><a href="<?php echo $this->createUrl('article/notice'); ?>" class="a1"> </a></li>
                    <li id="tb1_2" class="normaltab1" onmouseover="i:HoverLi1(2);"><a href="<?php echo $this->createUrl('article/news'); ?>" class="a2"> </a></li>
                    <li id="tb1_3" class="normaltab1" onmouseover="i:HoverLi1(3);"><a href="<?php echo $this->createUrl('article/knowledge'); ?>" class="a3"> </a></li>  
                    <li id="tb1_4" class="normaltab1" onmouseover="i:HoverLi1(4);"><a href="<?php echo $this->createUrl('article/activity'); ?>" class="a4"> </a></li>              
                </ul>
            </div> 
            <div class="ctt" id="list">
                <div class="dis" id="tbc1_01">
                    <ul>
                        <?php
                        foreach ($notices as $article) {
                            $timestamp = strtotime($article->create_time);
                            ?>
                            <li>
                                <div class="icon-date z">
                                    <div class="st1"><?php echo date('n月', $timestamp); ?></div>
                                    <div class="st2"><?php echo date('d', $timestamp); ?></div>
                                </div>
                                <div class="tit"><a href="<?php echo $this->createUrl('article/detail', array("id" => $article->id)) ?>"><?php echo mb_substr($article->title, 0, 50, 'utf-8'); ?></a></div>
                            </li>
                        <?php }
                        ?>
                    </ul>
                </div>
                <div class="undis" id="tbc1_02">
                    <ul>
                        <?php
                        foreach ($news as $article) {
                            $timestamp = strtotime($article->create_time);
                            ?>
                            <li>
                                <div class="icon-date z">
                                    <div class="st1"><?php echo date('n月', $timestamp); ?></div>
                                    <div class="st2"><?php echo date('d', $timestamp); ?></div>
                                </div>
                                <div class="tit"><a href="<?php echo $this->createUrl('article/detail', array("id" => $article->id)) ?>"><?php echo mb_substr($article->title, 0, 50, 'utf-8'); ?></a></div>
                            </li>
                        <?php }
                        ?>
                    </ul>
                </div>
                <div class="undis" id="tbc1_03">
                    <ul>
                        <?php
                        foreach ($knowledge as $article) {
                            $timestamp = strtotime($article->create_time);
                            ?>
                            <li>
                                <div class="icon-date z">
                                    <div class="st1"><?php echo date('n月', $timestamp); ?></div>
                                    <div class="st2"><?php echo date('d', $timestamp); ?></div>
                                </div>
                                <div class="tit"><a href="<?php echo $this->createUrl('article/detail', array("id" => $article->id)) ?>"><?php echo mb_substr($article->title, 0, 50, 'utf-8'); ?></a></div>
                            </li>
                        <?php }
                        ?>
                    </ul>
                </div>
                <div class="undis" id="tbc1_04">
                    <ul>
                        <?php
                        foreach ($activity as $article) {
                            $timestamp = strtotime($article->create_time);
                            ?>
                            <li>
                                <div class="icon-date z">
                                    <div class="st1"><?php echo date('n月', $timestamp); ?></div>
                                    <div class="st2"><?php echo date('d', $timestamp); ?></div>
                                </div>
                                <div class="tit"><a href="<?php echo $this->createUrl('article/detail', array("id" => $article->id)) ?>"><?php echo mb_substr($article->title, 0, 50, 'utf-8'); ?></a></div>
                            </li>
                        <?php }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="ad y" id="covers">
            <div id="cover">
                <div class="covers_container">
                    <?php
                    $isEmpty = true;
                    foreach ($covers as $cover) {
                        if (!empty($cover->image)){     
                            ?>
                        <div class="slide">
                            <a href="<?php
                            $isEmpty = false;
                            echo $this->createUrl('article/detail', array("id" => $cover->id))
                            ?>">
                                   <?php echo CHtml::image($this->baseUrl . $cover->image, "cover-封面", array('height' => '331', 'width' => '431')); ?>
                            </a>
                        </div>
                    <?php }} if ($isEmpty) { ?> 
                        <a href="javascript:;" style="cursor:text;"><?php echo CHtml::image($this->baseUrl . '/images/ad-index.png', "dynamic-ad", array('height' => '331', 'width' => '431')); ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="link ov">
    <div class="w1000">
        <div class="link-tit ov">合作伙伴</div>
        <div class="link-logo">
            <a href="javascript:;"><?php echo CHtml::image($this->baseUrl . "/images/link-logo1.gif", "picture"); ?></a>
            <a href="javascript:;"><?php echo CHtml::image($this->baseUrl . "/images/link-logo2.gif", "picture"); ?></a>
            <a href="javascript:;"><?php echo CHtml::image($this->baseUrl . "/images/link-logo3.gif", "picture"); ?></a>
            <a href="javascript:;"><?php echo CHtml::image($this->baseUrl . "/images/link-logo4.gif", "picture"); ?></a>
            <a href="javascript:;"><?php echo CHtml::image($this->baseUrl . "/images/link-logo5.gif", "picture"); ?></a>
            <a href="javascript:;"><?php echo CHtml::image($this->baseUrl . "/images/link-logo6.gif", "picture", array('style' => 'margin-right:0px')); ?></a>
            <a href="javascript:;"><?php echo CHtml::image($this->baseUrl . "/images/link-logo7.gif", "picture"); ?></a>
            <a href="javascript:;"><?php echo CHtml::image($this->baseUrl . "/images/link-logo8.gif", "picture"); ?></a>
            <a href="javascript:;"><?php echo CHtml::image($this->baseUrl . "/images/link-logo9.gif", "picture"); ?></a>
            <a href="javascript:;"><?php echo CHtml::image($this->baseUrl . "/images/link-logo10.gif", "picture"); ?></a>
            <a href="javascript:;"><?php echo CHtml::image($this->baseUrl . "/images/link-logo11.gif", "picture"); ?></a>
            <a href="javascript:;"><?php echo CHtml::image($this->baseUrl . "/images/link-logo12.gif", "picture", array('style' => 'margin-right:0px')); ?></a>
        </div>
    </div>
</div>

<script src="<?php echo Yii::app()->BaseUrl; ?>/js/slides.js" type='text/javascript'></script>

<script type='text/javascript'>
                    $(function() {

                        // Set starting slide to 1
                        var startSlide = 1;
                        // Get slide number if it exists
                        if (window.location.hash) {
                            startSlide = window.location.hash.replace('#', '');
                        }
                        // Initialize Slides
                        $('#slides').slides({
                            preload: false,
                            preloadImage: 'images/loading.gif',
                            generatePagination: true,
                            effect: 'fade',
                            play: 5000,
                            pause: 2500,
                            hoverPause: true,
                            // Get the starting slide
                            start: 1,
                            animationComplete: function(current) {
                                // Set the slide number as a hash
                                //window.location.hash = '#' + current;
                            }
                        });

                        $('#cover').slides({
                            preload: false,
                            preloadImage: 'images/loading.gif',
                            generatePagination: true,
                            container: 'covers_container',
                            paginationClass: 'paging',
                            effect: 'fade',
                            play: 5000,
                            pause: 2500,
                            hoverPause: true,
                            start: 1,
                            animationComplete: function(current) {
                            }
                        });
                    });
                    function g(o) {
                        return document.getElementById(o);
                    }
                    function HoverLi1(p) {
                        for (var a = 1; a <= 4; a++) {
                            g('tb1_' + a).className = 'normaltab1';
                            g('tbc1_0' + a).className = 'undis';
                        }
                        g('tbc1_0' + p).className = 'dis';
                        g('tb1_' + p).className = 'hovertab1';
                    }
                    function HoverLi2(q) {
                        for (var b = 1; b <= 3; b++) {
                            g('tb2_' + b).className = 'normaltab2';
                            g('tbc2_0' + b).className = 'undis';
                        }
                        g('tbc2_0' + q).className = 'dis';
                        g('tb2_' + q).className = 'hovertab2';
                    }
</script>
