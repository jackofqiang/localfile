<?php
$this->pageTitle = Yii::app()->name . '-网站地图';
  $cs = Yii::app()->getClientScript();
        $cs->registerCssFile(Yii::app()->baseUrl."/css/common927.css");
        $cs->registerCssFile(Yii::app()->baseUrl."/css/sitemap.css");
?>     
<div id="doc" alog-alias="doc">
    <div id="bd">
        <div class="section" id="产品中心"><h3 class="title"><b>产品中心</b></h3>
            <ul class="list clearfix">
                <li class="item"><a target="_blank" href="http://www.suncloud.cn/sunapp.html" class="link"><b>云平台</b></a></li>
                <li class="item"><a target="_blank" href="http://www.suncloud.cn/solution.html" class="link"><b>云方案</b></a></li>
                <li class="item"><a target="_blank" href="http://www.suncloud.cn/desktop.html" class="link"><b>云桌面</b></a></li>
            </ul>
        </div>
        <div class="section" id="生活服务"><h3 class="title"><b>阳光动态</b></h3>
            <ul class="list clearfix">
                <li class="item"><a target="_blank" href="http://www.suncloud.cn/article/index.html" class="link"><b>阳光动态</b></a></li>
                <li class="item"><a target="_blank" href="http://www.suncloud.cn/article/detail-28.html" class="link"><b>网站上线</b></a></li>
                <li class="item"><a target="_blank" href="http://www.suncloud.cn/article/detail-29.html" class="link"><b>维护公告</b></a></li>
            </ul>
        </div>
        <div class="section" id="成功案例"><h3 class="title"><b>成功案例</b></h3>
            <ul class="list clearfix">
                <li class="item"><a target="_blank" href="http://www.suncloud.cn/cases.html" class="link"><b>杨浦创业</b></a></li>
            </ul>
        </div>
        <div class="section" id="关于我们"><h3 class="title"><b>关于我们</b></h3>
            <ul class="list clearfix">
                <li class="item"><a target="_blank" href="http://www.suncloud.cn/about.html" class="link"><b>公司介绍</b></a></li>
                <li class="item"><a target="_blank" href="http://www.suncloud.cn/contact.html" class="link"><b>联系我们</b></a></li>
                <li class="item"><a target="_blank" href="http://www.suncloud.cn/jobs.html" class="link"><b>关于我们</b></a></li>
            </ul>
        </div>
    </div>
</div>