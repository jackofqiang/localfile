<div class="category">
    <h2>阳光动态</h2>
    <?php
    $this->widget('zii.widgets.CMenu', array(
        'items' => $this->menu,
        'activeCssClass' => 'on',
    ));
    ?>
</div>
<h3>产品推荐</h3>
<a href="<?php echo $this->createUrl('sunapp/'); ?>"><?php echo CHtml::image(Yii::app()->BaseUrl . "/images/left-ad1.jpg", "plat-ad", array('class' => 'mb13')); ?></a>
<a href="<?php echo $this->createUrl('solution/'); ?>"><?php echo CHtml::image(Yii::app()->BaseUrl . "/images/left-ad2.jpg", "solution-ad", array('class' => 'mb13')); ?></a>
<a href="<?php echo $this->createUrl('desktop/'); ?>"><?php echo CHtml::image(Yii::app()->BaseUrl . "/images/left-ad3.jpg", "desktop-ad"); ?></a>
