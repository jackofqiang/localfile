<?php
$this->pageTitle = Yii::app()->name . "--新闻列表";
$this->menu = array(
    array('label' => '', 'url' => array('notice'), 'linkOptions' => array('class' => 'a1')),
    array('label' => '', 'url' => array('news'), 'linkOptions' => array('class' => 'a2'),'active'=>true),
    array('label' => '', 'url' => array('knowledge'), 'linkOptions' => array('class' => 'a3')),
    array('label' => '', 'url' => array('activity'), 'linkOptions' => array('class' => 'a4')),
);
?>
<div class="banner ov">
    <div class="w1000"><?php echo CHtml::image(Yii::app()->BaseUrl . "/images/banner-arclist.gif", "picture"); ?></div>
</div>
<div class="pst">
    <div class="w1000 ov"><span style="margin-left:30px;"><a href="<?php echo $this->createUrl('index/'); ?>">首页</a></span><span style="background:none;"><a href="<?php echo $this->createUrl('article/index'); ?>">阳光动态</a></span></div>
</div>
<div class="w1000 ov">
    <div class="left z">
        <?php
    $this->renderPartial('_left');
    ?>
    </div>
    <div class="right y" >
        <h2>新闻</h2>
        <ul class="ullist">
            <?php
            foreach ($articles as $article) {
                $timestamp = strtotime($article->create_time);
                ?>
                <li><div class="icon-date z">
                        <div class="st1"><?php echo date('n月', $timestamp); ?></div>
                        <div class="st2"><?php echo date('d', $timestamp); ?></div>
                    </div>
                    <div class="tit z">
                        <h4>
                            <a href="<?php echo $this->createUrl('article/detail', array('id' => $article->id)); ?>"><?php echo $article->title; ?></a>
                        </h4>
                        <p><?php echo mb_substr($article->summary, 0, 100, 'utf-8'); ?></p>
                    </div>
                </li>
<?php } ?>
        </ul>
        <div class="pagebar ov ">
            <div id="pager" class="y">    
                <?php
                $this->widget('CLinkPager', array(
                    'header' => '',
                    'footer' => '',
                    'pages' => $pages,
                    'maxButtonCount' => 10,
                    "selectedPageCssClass" => 'on',
                    'cssFile' => Yii::app()->baseUrl . '/css/page.css',
                    'htmlOptions' => array(),
                        )
                );
                ?>    
            </div> 
        </div>
    </div>
</div>

