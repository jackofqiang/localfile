
<style>
    input.text { margin-bottom:12px; width:95%; padding: .4em; }
    fieldset { padding:0; border:0; margin-top:25px; }
    h1 { font-size: 1.2em; margin: .6em 0; }
    #upload-image{
        display:inline-block; 
        cursor:pointer;
        line-height:20px;
        width:40px;
        height:15px;
        font-size:15px;
        text-align:center;
        border-radius:4px;
        margin:0.2em 0 0.5em 0;
        margin-left:50px;
        border:1px solid green;
        padding:5px;
        word-spacing:3px;
    }
</style>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'article-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title', array('size' => 32, 'maxlength' => 32)); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'summary'); ?>
        <?php echo $form->textField($model, 'summary', array('size' => 60, 'maxlength' => 256)); ?>
        <?php echo $form->error($model, 'summary'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'category'); ?>
        <?php echo $form->dropDownList($model, 'category', $data); ?>
        <?php echo $form->error($model, 'category'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'content'); ?>
        <?php $this->widget('ext.ckeditor.CKEditor', array('model' => $model, 'attribute' => 'content', 'language' => 'zh_cn', 'editorTemplate' => 'full',)); ?>
        <?php echo $form->error($model, 'content'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'isrecommend', array('style' => 'display:inline;')); ?>
        <?php echo $form->radioButtonList($model, 'isrecommend', array('否', '是'), array('separator' => '&nbsp;', 'labelOptions' => array('style' => 'display:inline;'))); ?>
        <?php echo $form->error($model, 'isrecommend'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'image'); ?>
        <?php echo $form->textField($model, 'image'); ?><div id="upload-image">上传</div>
        <?php echo $form->error($model, 'image'); ?> 
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'iscover', array('style' => 'display:inline;')); ?>
        <?php echo $form->radioButtonList($model, 'iscover', array('否', '是'), array('separator' => '&nbsp;', 'labelOptions' => array('style' => 'display:inline;'))); ?>
        <?php echo $form->error($model, 'iscover'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'create_time'); ?>
        <?php
        $this->widget('ext.timepicker.timepicker', array(
            'model' => $model,
            'name' => 'create_time',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth' => true,
                'changeYear' => true,
                'yearRange' => '-30:+30'
            ),
        ));
        ?> 
        <?php echo $form->error($model, 'create_time'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'update_time'); ?>
        <?php
        $this->widget('ext.timepicker.timepicker', array(
            'model' => $model,
            'name' => 'update_time',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth' => true,
                'changeYear' => true,
                'yearRange' => '-30:+30'
            ),
        ));
        ?> 
        <?php echo $form->error($model, 'update_time'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? '创建' : '保存'); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->

<div id="dialog-form" title='图片上传'>
    <form action="<?php echo $this->createUrl('uploadImage'); ?>" method="post" enctype="multipart/form-data" target="ifr">
        <fieldset>
            <label for="filename">文件名</label>
            <input type="text" name="fname" id="filename" class="text ui-widget-content ui-corner-all" />
            <label for="image">图片</label>
            <input type="file" name="filedata" id="image" value="" class="text ui-widget-content ui-corner-all" />
            <input type="submit"  value="上传图片" class="text ui-widget-content ui-corner-all" />
            <input type='hidden' name="YII_CSRF_TOKEN" value="<?php echo Yii::app()->request->csrfToken; ?>">
        </fieldset>
    </form>
</div>

<script >
    $(function() {
        var file1 = $("#filename"),
                image = $("#image"),
                allFields = $([]).add(file1).add(image);
        $("#dialog-form").dialog({
            autoOpen: false,
            height: 300,
            width: 350,
            modal: true,
            buttons: {
                " 确 定 ": function() {
                    var bValid = true;
                    allFields.removeClass("ui-state-error")
                    if (bValid) {
                        $("#Article_image").val(file1.val());
                        $(this).dialog("close");
                    }
                },
                " 取 消 ": function() {
                    $(this).dialog("close");
                }
            },
            'close': function() {
                allFields.val("").removeClass("ui-state-error");
            }
        });

        $("#upload-image").click(function() {
            $("#dialog-form").dialog("open");
        });
    });

    function callback(msg) {
        $("input#filename").val(msg);
    }

</script>

<iframe name='ifr' style='display: none;' ></iframe>