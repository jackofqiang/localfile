<?php
$this->pageTitle = Yii::app()->name . "--通知详情";
$this->menu = array(
    array('label' => '', 'url' => array('notice'), 'linkOptions' => array('class' => 'a1'),'active'=>$model->category==6?true:false),
    array('label' => '', 'url' => array('news'), 'linkOptions' => array('class' => 'a2'),'active'=>$model->category==7?true:false),
    array('label' => '', 'url' => array('knowledge'), 'linkOptions' => array('class' => 'a3'),'active'=>$model->category==8?true:false),
    array('label' => '', 'url' => array('activity'), 'linkOptions' => array('class' => 'a4'),'active'=>$model->category==9?true:false),
);
?>
<div class="banner ov">
    <div class="w1000"><?php echo CHtml::image(Yii::app()->BaseUrl . "/images/banner-arclist.gif", "picture"); ?></div>
</div>

<div class="pst">
    <div class="w1000 ov"><span style="margin-left:30px;"><a href="<?php echo $this->createUrl('index/'); ?>">首页</a></span><span><a href="<?php echo $this->createUrl('article/index'); ?>">阳光动态</a></span><span  style="background:none;"><a href="javascript:;" style='cursor:text;'>动态正文</a></span></div>
</div>

<div class="w1000 ov">
    <div class="left z">
        <?php
        $this->renderPartial('_left');
        ?>
    </div>

    <div class="article right y">
        <?php if (is_object($model)) { ?>
            <h1><?php echo $model->title; ?></h1>
            <div class="time" style="border:none;padding-bottom:8px;">
                <?php
                list($date, ) = explode(" ", $model->create_time);
                echo $date;
                ?>          
            </div>
            <!-- Baidu Button BEGIN -->
            <div id="bdshare" class="bdshare_t bds_tools get-codes-bdshare" style="width:60%;height:30px;padding-left:40%;border-bottom:1px solid #eee;">
                <span class="bds_more">分享到：</span>
                <a class="bds_tsina"></a>
                <a class="bds_tqq"></a>
                <a class="bds_t163"></a>
                <a class="bds_qzone"></a>
            </div>
            <!-- Baidu Button END -->
            <div class="cont">
                <div class="desinfo">
                    <div class="intro">
    <?php echo $model->summary; ?>
                    </div>
                </div>
            <?php echo $model->content; ?>
            </div>
<?php } ?>
    </div>
</div>
<script type="text/javascript" id="bdshare_js" data="type=tools&amp;uid=1855138" ></script>
<script type="text/javascript" id="bdshell_js"></script>
<script type="text/javascript">
    document.getElementById("bdshell_js").src = "http://bdimg.share.baidu.com/static/js/shell_v2.js?cdnversion=" + Math.ceil(new Date() / 3600000);
</script>
