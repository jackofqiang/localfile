<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="阳光云,云计算,有孚,阳光互联,有孚网络,云平台,云方案,云桌面,私有云,解决方案,云主机"/>
        <meta name="description" content="阳光云是上海有孚计算机网络有限公司旗下品牌，是一个基于WEB服务整合了计算、存储与网络资源的弹性云技术Iaas平台。提供云平台、云方案、云桌面等云计算服务"/>
        <meta name="baidu-site-verification" content="cDi77Gf75G"/>
        <script src="<?php echo Yii::app()->BaseUrl; ?>/js/jquery-1.6.2.min.js"></script>
        <title><?php echo CHtml::encode($this->pageTitle); ?> - 基于WEB服务整合了计算、存储与网络资源的弹性云技术Iaas平台|云平台|云方案|云桌面 </title>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/cloud.css" />
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" />
    </head>
    <body>
        <div class="container" id="page">
            <div class="ding ov">
                <div class="w1000">
                    <iframe src="http://my.suncloud.cn/is_logged.html" frameBorder="0" scrolling="no" width="100%" height="100%" marginwidth="0" marginheight="0" hspace="0" vspace="0" allowTransparency="true" ></iframe>
                </div>
            </div>
            <div class="head w1000 ov">
                <div class="logo z"><a href="<?php echo $this->gameHost; ?>"><img src="<?php echo Yii::app()->baseUrl; ?>/images/logo.jpg" /></a></div>
                <!--备案icon start-->
                <div class="ba_icon y"><a href="http://icp.sundns.com/" target="_blank"><img src="images/ba_icon.gif" alt="备案" /></a></div>
                <!--备案icon end-->
                <div id="navMenu" class="y" >
                    <ul>
                        <li><a href='<?php echo $this->gameHost; ?>/index.html'>主页</a></li>
                        <li><a href='<?php echo $this->gameHost; ?>/host.html'  rel='dropmenu1' >产品＆服务</a></li>
                        <li  class="onelink"><a href="javascript:void(0);"  rel='dropmenu2' >行业云</a></li>
                        <li><a href='<?php echo $this->gameHost; ?>/suport.html' rel="dropmenu3" >技术支持</a></li>
                        <li><a href='<?php echo $this->gameHost; ?>/about-company.html'  rel='dropmenu4' >关于我们</a></li>
                    </ul>     
                </div>
                <script type='text/javascript' src="<?php echo Yii::app()->baseUrl; ?>/js/menu.js"></script>
                <ul id="dropmenu1" class="dropMenu" >
                    <li><dl style="height:50px;margin-bottom:20px; border-bottom:1px dotted #aaa; line-height:35px;"><dt>计算和网络</dt><dd><A href="<?php echo $this->gameHost; ?>/host.html">云主机</A><A href="<?php echo $this->gameHost; ?>/balance.html">负载均衡</A><A href="<?php echo $this->gameHost; ?>/defend.html">云防护</A></dd></dl></li>
                    <li><dl><Dt>存储和CDN</Dt>
                            <dd class="amb"><A href="<?php echo $this->gameHost; ?>/disk.html" >云硬盘</A><A href="<?php echo $this->gameHost; ?>/database.html">云数据库</A><div class="cl ov" style="height:0"></div><A href="<?php echo $this->gameHost; ?>/memory.html">云内存存储</A><A href="<?php echo $this->gameHost; ?>/distribute.html">云分发CDN</A></dd></dl></li>
                </ul>

                <ul id="dropmenu2" class="dropMenu">
                    <li><a href="/" target="_blank">游戏云</a></li>
                    <li><a href="<?php echo $this->createUrl('sunapp/'); ?>" target="_blank">阳光云平台</a></li>
                    <li><a href="<?php echo $this->createUrl('solution/'); ?>" target="_blank">阳光云方案</a></li>
                    <li><a href="<?php echo $this->createUrl('desktop/'); ?>" target="_blank">阳光云桌面</a></li>
                </ul>


                <ul id="dropmenu3" class="dropMenu">
                    <li><a href="http://docs.suncloud.cn/" target="_blank">产品说明手册</a></li>
                </ul>

                <ul id="dropmenu4" class="dropMenu">
                    <li><a href="<?php echo $this->gameHost; ?>/about-company.html" >公司介绍</a></li>
                    <li><a href="<?php echo $this->gameHost; ?>/about-contact.html" >联系我们</a></li>
                </ul>
                <script type="text/javascript">cssdropdown.startchrome("navMenu")</script> 

            </div>

            <?php echo $content; ?>
            <div class="clear"></div>
            <div id="foot" class="ov">
                <div class="w1000">
                    <div class="ft-lg z"><img src="images/ft-lg.gif" /></div>
                    <div class="y">
                        <ul class="ulprd" style="width:210px;">
                            <li class="tit" style="width:120px;padding-right:80px;"><a href="<?php echo $this->gameHost; ?>/host.html">产品＆服务</a></li>
                            <li><a href="<?php echo $this->gameHost; ?>/host.html">云主机</a></li>
                            <li><a href="<?php echo $this->gameHost;
            ; ?>/balance.html">负载均衡</a></li>
                            <li><a href="<?php echo $this->gameHost; ?>/disk.html">云硬盘</a></li>
                            <li><a href="<?php echo $this->gameHost; ?>/defend.html">云防护</a></li>
                            <li><a href="<?php echo $this->gameHost; ?>/memory.html">云内存存储</a></li>
                            <li><a href="<?php echo $this->gameHost; ?>/distribute.html">云分发CDN</a></li>
                            <li><a href="<?php echo $this->gameHost; ?>/database.html">云数据库</a></li>
                            <li><a href="http://my.suncloud.cn/#/dns_domain">DNS</a></li>
                        </ul>
                        <ul>
                            <li class="tit"><a href="javascript:void(0);">行业云</a></li>
                            <li><a href="<?php echo $this->gameHost; ?>" target="_blank">游戏云</a></li>
                            <li><a href="<?php echo $this->createUrl('sunapp/'); ?>" target="_blank">阳光云平台</a></li>
                            <li><a href="<?php echo $this->createUrl('solution/'); ?>" target="_blank">阳光云方案</a></li>
                            <li><a href="<?php echo $this->createUrl('desktop/'); ?>" target="_blank">阳光云桌面</a></li>
                        </ul>
                        <ul>
                            <li class="tit"><a href="<?php echo $this->gameHost; ?>/suport.html">技术支持</a></li>
                            <li><a href="http://docs.suncloud.cn/" target="_blank">产品说明手册</a></li>
                        </ul>
                        <ul>
                            <li class="tit"><a href="<?php echo $this->gameHost; ?>/about-company.html">关于我们</a></li>
                            <li><a href="<?php echo $this->gameHost; ?>/about-company.html" >公司介绍</a></li>
                            <li><a href="<?php echo $this->gameHost; ?>/about-contact.html" >联系我们</a></li>
                            <li><a href="<?php echo $this->gameHost; ?>" target="_blank">阳光云suncloud</a></li>
                            <li><a href="http://www.sundns.com/" target="_blank">阳光互联</a></li>
                        </ul>
                        <div class="cl"></div>
                        <p>Copyright ©2013 上海有孚计算机网络有限公司 沪B1.B2-20040073</p>
                    </div>
                </div>
            </div>
            <div style="display:none;">
                <script type="text/javascript">
                    var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
                    document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F23e0a70d96dab9450f603b2e730e84a6' type='text/javascript'%3E%3C/script%3E"));
                </script>
            </div>
        </div>
    </body>
</html>

