<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl; ?>/css/screen.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl; ?>/css/print.css" media="print" />
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl; ?>/css/form.css" />
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->baseUrl; ?>/favicon.ico" />
        <script src="<?php echo Yii::app()->BaseUrl; ?>/js/jquery-1.6.2.min.js"></script>
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>
    <body>
        <div class="container" id="page">
            <div id="header">
                <div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
            </div><!-- header -->
            <div id="mainmenu">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items' => array(
                        array('label' => '阳光动态', 'url' => array('article/admin')),
                        array('label' => '需求-云方案', 'url' => array('message/admin')),
                        array('label' => '联系-云桌面', 'url' => array('contact/admin')),
                        array('label' => '客户案例', 'url' => array('cases/admin')),
                        array('label' => '招聘管理', 'url' => array('jobs/admin')),
                        array('label' => '关于我们', 'url' => array('about/admin')),
                        array('label' => 'Apps', 'url' => array('app/admin')),
                        array('label' => '类型管理', 'url' => array('enum/admin')),
                        array('label' => 'Login', 'url' => array('yovoleAdmin/login'), 'visible' => Yii::app()->user->isGuest),
                        array('label' => 'Logout (' . Yii::app()->user->name . ')', 'url' => array('yovoleAdmin/logout'), 'visible' => !Yii::app()->user->isGuest)
                    ),
                ));
                ?>
            </div><!-- mainmenu -->
            <?php
            $this->widget('zii.widgets.CBreadcrumbs', array(
                'homeLink' => CHtml::link('Home', array('yovoleAdmin/')),
                'links' => $this->breadcrumbs,
            ));
            ?><!-- breadcrumbs -->
            <?php echo $content; ?>
            <div style="clear:both"></div>
            <div id="footer">
                Copyright &copy;
                <?php
                @ini_set('date.timezone', 'Asia/Shanghai');
                echo date('Y');
                ?> by 上海有孚计算机网络有限公司 .<br/>
                All Rights Reserved.<br/>
                <?php echo Yii::YFpowered(); ?>
            </div><!-- footer -->
        </div><!-- page -->
    </body>
</html>