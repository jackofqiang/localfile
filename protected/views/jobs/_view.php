<?php
/* @var $this JobsController */
/* @var $data Jobs */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jobtitle')); ?>:</b>
	<?php echo CHtml::encode($data->jobtitle); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jobaddr')); ?>:</b>
	<?php echo CHtml::encode($data->jobaddr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('requirements')); ?>:</b>
	<?php echo CHtml::encode($data->requirements); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('responsibilities')); ?>:</b>
	<?php echo CHtml::encode($data->responsibilities); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('post_author')); ?>:</b>
	<?php echo CHtml::encode($data->post_author); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('post_time')); ?>:</b>
	<?php echo CHtml::encode($data->post_time); ?>
	<br />


</div>