<?php
/* @var $this JobsController */
/* @var $model Jobs */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'jobs-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'jobtitle'); ?>
		<?php echo $form->textField($model,'jobtitle',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'jobtitle'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jobaddr'); ?>
		<?php echo $form->textField($model,'jobaddr',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'jobaddr'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($model,'num'); ?>
		<?php echo $form->textField($model,'num'); ?>
		<?php echo $form->error($model,'num'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'requirements'); ?>
		<?php echo $form->textArea($model,'requirements',array('cols'=>120,'rows'=>30)); ?>
		<?php echo $form->error($model,'requirements'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'post_author'); ?>
		<?php echo $form->textField($model,'post_author',array('size'=>0,'maxlength'=>0)); ?>
		<?php echo $form->error($model,'post_author'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'post_time'); ?>
		<?php echo $form->textField($model,'post_time'); ?>
		<?php echo $form->error($model,'post_time'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->