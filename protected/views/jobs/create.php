<?php
/* @var $this JobsController */
/* @var $model Jobs */

$this->breadcrumbs=array(
	'Jobs'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Jobs', 'url'=>array('admin')),
	array('label'=>'Manage Jobs', 'url'=>array('admin')),
);
?>

<h1>Create Jobs</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>