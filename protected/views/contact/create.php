<?php
/* @var $this ContactController */
/* @var $model Contact */

$this->breadcrumbs=array(
	'Contacts'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Contact', 'url'=>array('admin')),
	array('label'=>'Manage Contact', 'url'=>array('admin')),
);
?>

<h1>Create Contact</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>