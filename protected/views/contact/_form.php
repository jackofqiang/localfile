<?php
/* @var $this ContactController */
/* @var $model Contact */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>


	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'individual_duties'); ?>
		<?php echo $form->textField($model,'individual_duties',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'individual_duties'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'company'); ?>
		<?php echo $form->textField($model,'company',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'company'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'mobile'); ?>
		<?php echo $form->textField($model,'mobile',array('size'=>20,'maxlength'=>12)); ?>
		<?php echo $form->error($model,'mobile'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'addr'); ?>
		<?php echo $form->textField($model,'addr',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'addr'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'demandnum'); ?>
		<?php echo $form->textField($model,'demandnum',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'demandnum'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'addr_more'); ?>
		<?php echo $form->textField($model,'addr_more',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'addr_more'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'remark'); ?>
		<?php echo $form->textField($model,'remark',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'remark'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->