

ALTER TABLE `cloud_enum` ADD COLUMN `order` TINYINT(4) DEFAULT 1;

ALTER TABLE cloud_contact ADD COLUMN `demandnum` INT(11) DEFAULT 50 COMMENT '需求数量' AFTER `company`;
