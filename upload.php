<?php

if ($_FILES["filedata"]["error"] > 0) {
    echo "Error: " . $_FILES["filedata"]["error"] . "<br />";
} else {
    $dir = "upload" . date('/m/d/');
    if (!is_dir($dir)) {
        mkdir($dir, 0777, true);
    }
    $file = uniqid() . '_' . $_FILES["filedata"]["name"];
    $path = $dir . $file;
    move_uploaded_file($_FILES["filedata"]["tmp_name"], $path);
    echo<<<js
  <script>
	parent.callback('$path');
  </script>
js;
}
?>