/* 
 * 
 * counter plugin
 */
(function($, undefined) {
    $.fn.counter = function(option) {//此行中的$为传进来的参数jQuery
        var options = {id:'#mydialog',step: 1000, context: window,start:3}, self = this;
        options = $.extend(options, option);//此行中的$为传进来的参数jQuery
        self.text(options.start);
        var counter = setInterval(function() {
            var txt=self.text();
            self.text(parseInt(txt) - 1);
            if ((parseInt(txt) - 1) < 1) {
                clearInterval(counter);
                options.context.$(options.id).dialog("close");//此行中的$非传进来的参数jQuery
            }
        }, options.step);
    }
})(jQuery);


